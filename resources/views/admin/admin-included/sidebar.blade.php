<section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="{{ Request::segment(1) == 'dashboard'? 'active' : '' }}">
          <a href="{{url('/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container"> 
            </span>
          </a>
        </li>
        <li class="{{ Request::segment(1) == 'admin'? 'active' : '' }}">
          <a href="{{ url('/admin') }}">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
        </li>
        <li class="{{ Request::segment(1) == 'room'? 'active' : '' }}">
          <a href="{{ url('/room/index') }}">
            <i class="fa fa-bed"></i>
            <span>Rooms</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
        </li>
        <li class="{{ Request::segment(1) == 'walkin-reservation'? 'active' : '' }}">
          <a href="{{ url('/walkin-reservation/index') }}">
            <i class="fa fa-bed"></i>
            <span>Walkin Reservation</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::segment(1) == 'system-settings'? 'active' : '' }}"><a href="/system-settings"><i class="fa fa-gear"></i> System Settings</a></li>
            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>






      </ul>
    </section>