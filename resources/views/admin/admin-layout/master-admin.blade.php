<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Delivery Admin</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="../../custom/portlet.css" rel="stylesheet">
        <link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        @yield('page-css')
        <style type="text/css">
            .btn {cursor: pointer;}
            #map {
                height: 500px;
                width:80%;
                float:left;
              }
        </style>
    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
                <header class="main-header">
                    @include('admin.admin-included.header')
                </header>
                <aside class="main-sidebar">
                    @include('admin.admin-included.sidebar')
                </aside>
                <div class="content-wrapper" style="background: white;">
                @yield('content')
                </div>
                <footer class="main-footer">
                    @include('admin.admin-included.footer')
                </footer>
                <aside class="control-sidebar control-sidebar-dark">
                    @include('admin.admin-included.control')
                </aside>
                 <div class="control-sidebar-bg"></div>
    </div>
        <script src="../../bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="../../bower_components/fastclick/lib/fastclick.js"></script>
        <script src="../../dist/js/adminlte.min.js"></script>
        <script src="../../dist/js/demo.js"></script>
        <script src="../../bower_components/moment/min/moment.min.js">  </script>
    <script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
        @yield('page-scripts')
        <script type="text/javascript">
            var token = "{{csrf_token()}}";
            $('body').delegate('.logout','click',function(){
                $.post("{{route('logout')}}",{_token:token},function(){
                    window.location.reload();
                });
            });
            setTimeout(function() {
            $(".alert").slideUp(500, function(){
                $('.alert').css('display','none'); 
                });
            }, 1000);
        </script>
    </body>
</html>