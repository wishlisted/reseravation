@extends('admin.admin-layout.master-admin')
@section('page-css')

	<link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<style type="text/css">
  		.link2{
  			cursor: pointer;
  		}
  	</style>


@stop

@section('content')
<div class="row" style="margin-top: 50px;">
		<div class="col-md-12">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2"><h3 class="box-title">Sales Chart</h3></div>
					<div class ="col-md-2 col-md-offset-4" >
						<div id="options" class="btn-group" role="group" aria-label="Basic example">
						  <button id="daily" type="button" class="btn btn-secondary">Daily</button>
						  <button id="monthly" type="button" class="btn btn-secondary">Monthly</button>
						</div>
					</div>
					<div id="month" class="col-md-2">
						<div id ="span_payment_method" class ="form-group remove">
							<select class ="form-control" name="month">
								<option value="1">January</option>
							 	<option value="2">February</option>
							 	<option value="3">March</option>
							 	<option value="4">April</option>
							 	<option value="5">May</option>
							 	<option value="6">June</option>
							 	<option value="7">July</option>
							 	<option value="8">August</option>
							 	<option value="9">September</option>
							 	<option value="10">October</option>
							 	<option value="11">November</option>
							 	<option value="12">December</option>
							</select>
						</div>
					</div>
					<div id="year" class="col-md-2" style="padding-right: 50px;">
						<div id ="span_payment_method" class ="form-group remove">
							<select class ="form-control" name="year">
								@foreach($year as $yr)
									<option value="{{ $yr->y }}">{!! $yr->y !!}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<canvas id="myChart" width="400" height="150"></canvas>
			</div>
			<div class="col-md-7" style="margin-top: 30px;">
				<div class="box box-info">
		            <div class="box-header with-border">
		              <h3 class="box-title">Available Rooms</h3>

		              <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		                <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		              </div>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <div class="table-responsive">
		                <table class="table table-striped table-bordered table-hover" id ="tbl_available">
		                  <thead>
		                  <tr>
		                    <th>Room Number</th>
		                    <th>Room Type</th>
		                    <th>Capacity</th>
		                    <th>Number Of Bed</th>
		                    <th>Rate</th>
		                  </tr>
		                  </thead>
		                  <tbody>
		                  	<?php //dd($rooms); ?>
		                  @foreach($rooms as $rm)
		                  <tr  class ="link" data-id ="{!! $rm->room_type_id !!}">
		                    <td>{{ $rm->room_no }}</td>
		                    <td><span class="label label-success" style="text-transform: uppercase;">{{ $rm->name }}</span></td>
		                    <td>{{ $rm->capacity }}</td>
		                    <td>{{ $rm->bed }}</td>
		                    <td>{{ $rm->rate }}</td>
		                  </tr>
		                  @endforeach
		                  </tbody>
		                </table>
		              </div>
		              <!-- /.table-responsive -->
		            </div>
		            <!-- /.box-body -->
		            <div class="box-footer clearfix">
		              <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
		              <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
		            </div>
		            <!-- /.box-footer -->
		          </div>
			</div>
			<div class="col-md-5" style="margin-top: 30px;">
				<div class="box box-info">
		            <div class="box-header with-border">
		              <h3 class="box-title">Occupied Rooms</h3>

		              <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		                <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		              </div>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <div class="table-responsive">
		                <table  class="table table-striped table-bordered table-hover" id ="tbl_occupied">
		                  <thead>
		                  <tr>
		                    <th>Room Number</th>
		                    <th>Room Type</th>
		                    <th>Status</th>
		                    <!-- <th>Capacity</th>
		                    <th>Number Of Bed</th>
		                    <th>Size</th>
		                    <th>Rate</th> -->
		                  </tr>
		                  </thead>
		                  <tbody>
		                  @foreach($rooms2 as $rm)
		                  <tr class ="link2" data-id ="{!! $rm->id !!}">
		                    <td>{{ $rm->room_no }}</td>
		                    <td><span class="label label-success"  style="text-transform: uppercase;">{{ $rm->name }}</span></td>
		                    <td><span class="label label-warning"  style="text-transform: uppercase;"> active </span></td>
		                    <!-- <td>{{ $rm->capacity }}</td>
		                    <td>{{ $rm->bed }}</td>
		                    <td>{{ $rm->size }}</td>
		                    <td>{{ $rm->rate }}</td> -->
		                  </tr>
		                  @endforeach
		                  </tbody>
		                </table>
		              </div>
		              <!-- /.table-responsive -->
		            </div>
		            <!-- /.box-body -->
		            <div class="box-footer clearfix">
		              <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
		              <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
		            </div>
		            <!-- /.box-footer -->
		          </div>
			</div>
		</div>
</div>
@stop

@section('page-scripts')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	

	<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
jQuery(document).ready(function() {  
	// ChartsFlotcharts.initCharts();

	$('#tbl_available').DataTable()
	$('#tbl_occupied').DataTable()
	
	$('.link').click(function(){
		var id = $('.link').data('id')
		window.location = '/room/form/'+id
	})
	$('.link2').click(function(){
		var id = $('.link2').data('id')
		window.location = '/walkin-reservation/form/'+id
	})
	var type = 1;
	$('#monthly').css("background","none");
	function sales(type){
		$.ajax({
	        url: "dashboard/sales",
	        method:'GET',
	        data:{
	            year:$('select[name="year"]').val(),
	            type:type,
	            month:$('select[name="month"]').val(),
	        },
	        success:function(data){
	        	console.log(data,);
	        	var canvas = document.getElementById('myChart');
				var data = {
				    labels: data.label,
				    datasets: [
				        {
				            label: "Sales dataset",
				            fill: false,
				            lineTension: 0.1,
				            backgroundColor: "rgba(75,192,192,0.4)",
				            borderColor: "rgba(75,192,192,1)",
				            borderCapStyle: 'butt',
				            borderDash: [],
				            borderDashOffset: 0.0,
				            borderJoinStyle: 'miter',
				            pointBorderColor: "rgba(75,192,192,1)",
				            pointBackgroundColor: "#fff",
				            pointBorderWidth: 1,
				            pointHoverRadius: 5,
				            pointHoverBackgroundColor: "rgba(75,192,192,1)",
				            pointHoverBorderColor: "rgba(220,220,220,1)",
				            pointHoverBorderWidth: 2,
				            pointRadius: 5,
				            pointHitRadius: 10,
				            data: data.data,
				        }
				    ]
				};

				var option = {
					showLines: true
				};

				myLineChart = new Chart.Line(canvas,{
					data:data,
				  	options:option
				});

	        },error:function(e){
	            // alert('error')
	        }
	    });
	}
	var myLineChart = '';
	sales(type);
	$('#monthly').on('click', function()
    {	
    	myLineChart.destroy();
    	$('select[name="month"]').attr("disabled", true); 
    	// $('#month').css("display","none");
    	$('#daily').css("background","none");
    	$(this).css("background","rgb(221, 221, 221)");

        type = 2;
        sales(type);
    });
	$('#daily').on('click', function()
    {	
    	myLineChart.destroy();
    	$("select[name='month']").removeAttr("disabled");
    	// $('#month').css("display","block");
    	$('#monthly').css("background","none");
    	$(this).css("background","rgb(221, 221, 221)");

        type = 1;
        sales(type);
    });
	$('select[name="month"]').on('change', function()
    {	
    	myLineChart.destroy();
    	sales(type);
    });
	$('select[name="year"]').on('change', function()
    {	
    	myLineChart.destroy();
    	sales(type);
    });




});
</script>
@stop

