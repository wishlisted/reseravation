<?php use App\User; ?>
@extends('admin.admin-layout.master-admin')

@section('page-css')
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<section class ="content">
    @include('alerts.alert-msg')
    <div class="row" id ="follow">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject text-uppercase"> {{$type}} List </span>
                    </div>
                    <div class="actions">
                        <a href="{{url('/admin')}}" class="btn btn-default btn-flat">
                            <i class="fa fa-arrow-left"></i> Back
                        </a>
                        <button href="#" class="btn btn-danger btn-flat" id="btnDelete">
                            <i class="fa  fa-trash-o"></i> Delete
                        </button>
                    </div>
                </div>
                <div class="portlet-body" style="padding: 0 15px">
                    <form id="frmDelete" role="form">  {!! csrf_field() !!}
                      	<table id ="dataTable"  class="table table-hover">
                            <thead>
                                <tr class="table-light">
                                    <th> </th>
                                    <th>#</th>
                                    <th>Username </th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Status<th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $us)
                                <tr class="link" id="0">
                                    <td><span class ="align-middle"><input type ="checkbox" name="check_delete[]" class ="ids" value="{{$us->id}}"></span></td>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$us->username}}</td>
                                    <td>{{$us->email}}</td>
                                    <td>{{$us->mobile_no}}</td>
                                    <td>{{$us->status}}<td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-scripts')
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    $(document).ready(function(){
        $('#dataTable').DataTable()
    })
    $('#btnDelete').off('click').on('click', function(){
        var confirms = confirm('Are you sure you want to Delete this?')
        if(confirms == true){
            if($('#frmDelete input[name="check_delete[]"]:checked').length > 0){
                var items = [];
                $('.ids').each(function(){
                    if($(this).is(':checked')){
                        items.push($(this).val());
                    }
                });
                console.log(items)
                $.ajax({
                    url: "/admin/delete",
                    method: "POST",
                    dataType: "JSON",
                    data:{
                        id: items
                    },
                    success : function (e){
                        if(e.status == 1){
                            location.reload()
                        }
                    }
                })

            } else {
            alert('Please select atleast 1 item')
            }
        }

    })
    setTimeout(function() {
        $(".alert").slideUp(500, function(){
            $('.alert').css('display','none'); 
        });
    }, 1000);
</script>
@stop
