<?php use App\User; ?>
@extends('admin.admin-layout.master-admin')

@section('page-css')
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<section class ="content">
    @include('alerts.alert-msg')
    <div class="row" id ="follow">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject text-uppercase"> Register Admin User </span>
                    </div>
                    <div class="actions">
                        <a href="{{url('/admin')}}" class="btn btn-default btn-flat">
                            <i class="fa fa-arrow-left"></i> Back
                        </a>
                        <button href="#" class="btn btn-primary btn-flat" id="btnSave">
                            <i class="fa fa-save"></i> Save 
                        </button>
                    </div>
                </div>
                <div class="portlet-body" style="padding: 0 15px">
                    <form id="frmSave">
                      <div clas="row">
                        <div class ="col-md-4">
                            <div id ="span_username" class ="form-group remove">
                                <h4><span style="color:red;">*</span>Username:</h4>
                                <input type="text" class ="form-control username" name="username" value="" style="">
                                <label></label>
                            </div>
                        </div>
                        <div class ="col-md-4">
                            <div id ="span_password" class ="form-group remove">
                                <h4><span style="color:red;">*</span>Password:</h4>
                                <input type="password" class ="form-control password" name="password" value="" style="">
                                <label></label>
                            </div>
                        </div>
                        <div class ="col-md-4">
                            <div id ="span_confirm_password" class ="form-group remove">
                                <h4><span style="color:red;">*</span>Confirm Password:</h4>
                                <input type="password" class ="form-control confirm_password" name="confirm_password" value="" style="">
                                <label></label>
                            </div>
                        </div>
                      </div>
                      <div clas="row">
                        <div class ="col-md-4">
                            <div id ="span_first_name" class ="form-group remove">
                                <h4><span style="color:red;">*</span>firstname:</h4>
                                <input type="text" class ="form-control first_name" name="first_name" value="" style="text-transform: uppercase;">
                                <label></label>
                            </div>
                        </div>
                        <div class ="col-md-4">
                            <div id ="span_lastname" class ="form-group remove">
                                <h4><span style="color:red;"></span>lastname:</h4>
                                <input type="text" class ="form-control last_name" name="last_name" value="" style="text-transform: uppercase;">
                                <label></label>
                            </div>
                        </div>
                        <div class ="col-md-4">
                            <div id ="span_email_address" class ="form-group remove">
                                <h4><span style="color:red;"></span>Email:</h4>
                                <input type="email" class ="form-control email_addess" name="email_address" value="" style="text-transform: uppercase;">
                                <label></label>
                            </div>
                        </div>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-scripts')
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$('.link').click(function(){
		var id = $(this).attr('id')
		window.location = "/admin/users/"+id
	})
    $('#btnSave').on('click', function(){
        var input = $('#frmSave').serializeArray()
        var formData = new FormData()
        $.each(input,function(key,input){
            formData.append(input.name,input.value);
        });
         $.ajax({
            url: "save",
            type: "POST",
            dataType: "json",
            contentType: false,
            processData: false,
            cache: false,
            data: formData,
            success: function(e){
                if(e.status == 1){
                    location.reload()
                }
                else if(e.status == 2){
                    location.reload()
                }
                else{
                    $('.remove').find('label').html('')
                    $('.remove').removeClass('has-error')
                    $.each(e.errors,function(key, data){
                        if(data){
                            $('#span_'+key).find('label').html(data)
                            $('#span_'+key).addClass('has-error')
                        }
                    })
                }
            }
         })
    })
    setTimeout(function() {
        $(".alert").slideUp(500, function(){
            $('.alert').css('display','none'); 
        });
    }, 1000);
</script>
@stop
