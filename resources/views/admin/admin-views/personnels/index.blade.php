<?php use App\User; ?>
@extends('admin.admin-layout.master-admin')

@section('page-css')
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<section class ="content">
    @include('alerts.alert-msg')
    <div class="row" id ="follow">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject text-uppercase"> Users Information</span>
                    </div>
                    <div class="actions">
                        <a href="{{url('/dashboard')}}" class="btn btn-default btn-flat">
                            <i class="fa fa-arrow-left"></i> Back
                        </a>
                        <a href="{{url('/admin/register')}}" class="btn btn-success btn-flat">
                            <i class="fa fa-plus"></i> Add Admin User
                        </a>
                    </div>
                </div>
                <div class="portlet-body" style="padding: 0 15px">
                  	<table id ="dataTable"  class="table table-hover">
                            <thead>
                                <tr class="table-light">
                                	<th>#</th>
                                	<th>User Type</th>
                                	<th> Number of Users</th>

                                </tr>
                            </thead>
                            <tbody>

                                <tr class="link" id="0">
                                	<td>1</td>
                                	<td>Admin</td>
                                	<td><?php echo DB::table('users')->where('type', 0)->count() ;?></td>
                                </tr>
                                <tr class="link" id ="1">
                                	<td>2</td>
                                	<td>Customers</td>
                                	<td><?php echo DB::table('users')->where('type', 1)->count() ;?></td>
                                </tr>

                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-scripts')
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	$('.link').click(function(){
		var id = $(this).attr('id')
		window.location = "/admin/users/"+id
	})
</script>
@stop
