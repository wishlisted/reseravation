@extends('admin.admin-layout.master-admin')
@section('page-css')
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@stop
@section('content')
<section class ="content">
	@include('alerts.alert-msg')
	<div class="row" id ="follow">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject text-uppercase"> System Settings</span>
					</div>
					<div class="actions">
						<a href="{{url('/dashboard')}}" class="btn btn-default btn-flat">
							<i class="fa fa-arrow-left"></i> Back
						</a>
						<button id="btn-save" class="btn btn-primary btn-flat">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
				</div>
				<div class="portlet-body" style="padding: 0 15px">
					<div class="col-md-12" style="">
						<form id="frmAdd"> {{csrf_field()}}
							<div class="col-md-12">
								<div class="row">
				                    <div class="col-md-2">
				                        <div class="form-group">
				                            <label style="color: black; margin-bottom: 10px;">Breakfast Price</label>
			                                <input type="number" class ="form-control" placeholder="Breakfast Price" name="breakfastprice" value="{{ $system->allow_free_breakfast }}">
				                        </div>
				                    </div>
				                    <div class="col-md-2">
				                        <div class="form-group">
				                            <label style="color: black; margin-bottom: 10px;">Check-in</label>
			                                <!-- <input type="text" class ="form-control" placeholder="Check-in Date" name="checkin" value="{{ $system->checkin }}" data-format="hh:mm:ss"> -->
			                                <select class ="form-control" name="checkin">
			                                </select>
				                        </div>
				                    </div>
				                    <div class="col-md-2">
				                        <div class="form-group">
				                            <label style="color: black; margin-bottom: 10px;">Check-out</label>
			                                <!-- <input type="text" class ="form-control" placeholder="Check-out Date" name="checkout" value="{{ $system->checkout }}" data-format="hh:mm:ss"> -->
			                                <select class ="form-control" name="checkout">
			                                </select>
				                        </div>
				                    </div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-scripts')
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">


// $('input[name="checkin"]').datepicker({
// 	autoclose: true,
// 	format: 'HH:mm:ss', pickDate:false 
// })
// $('input[name="checkout"]').datepicker({
// 	autoclose: true,
// 	format: 'HH:mm:ss', pickDate:false 
// })
var time1 = '{{ $system->checkin }}';
var time2 = '{{ $system->checkout }}';
var hours = 0;
var data1 ;
var data2 ;
var selected1 = '';
var selected2 = '';
for (var i = 0; i <= 24; i++) {
	hours = i;
	if (hours.toString().length <= 1) {
		hours = '0'+hours;
	}
	if (hours+':00:00' == time1) {
		selected1 = 'selected';
	}else{
		selected1 = '';
	}
	if (hours+':00:00' == time2) {
		selected2 = 'selected';
	}else{
		selected2 = '';
	}
	data1 += '<option value="'+hours+':00:00" '+selected1+'>'+hours+':00:00</option>';
	data2 += '<option value="'+hours+':00:00" '+selected2+'>'+hours+':00:00</option>';
}

$('select[name="checkin"]').html(data1);
$('select[name="checkout"]').html(data2);

$("#btn-save").click(function(){
	id = "{!! !empty($system)? $system->id : '1' !!}";
	price = $('input[name="breakfastprice"]').val();
	checkin = $('select[name="checkin"]').val();
	checkout = $('select[name="checkout"]').val();
	// console.log(price,checkin,checkout)
	$.ajax({
        url: "{!! url('system-settings/save') !!}" +'/'+id,
        type: "get",
        dataType: "json",
        data: {price:price,checkin:checkin,checkout:checkout},
        success:function(data){
        	location.reload();
        },error:function(e){
            //console.log(e);
        }
    });
});

</script>

	
@stop

