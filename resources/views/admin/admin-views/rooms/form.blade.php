@extends('admin.admin-layout.master-admin')
@section('page-css')

@stop

@section('content')
<section class ="content">
	@include('alerts.alert-msg')
	<div class="row" id ="follow">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject text-uppercase"> Room Maintenance</span>
					</div>
					<div class="actions">
						<a href="{{url('/room/index')}}" class="btn btn-default btn-flat">
							<i class="fa fa-arrow-left"></i> Back
						</a>
						<button href="#" class="btn btn-primary btn-flat" id="btnSave">
							<i class="fa fa-save"></i> Save 
						</button>
					</div>
				</div>
				<div class="portlet-body" style="padding: 0 15px">
					<form id="frmSave" role="form">  {!! csrf_field() !!}
						<div class ="row">
							<div class ="col-md-3 hover" style="padding:0">
								<?php
								if(!empty($info->image)){
									$src = "../../uploads/room_images/".$info->image;
								} else {
									$src = "../../images/rooms/not-available.jpg";
								}
								?>
								<a class ="profile-image" href ="javascript:;">
									<img id="image_preview" src="{{ url($src) }}" width="100%" height="200"  style="border:2px solid #42c7ce; margin-bottom: 10px;">
									<div class ="overlay">	
										<div class ="overlay-content">
											<div class ="visibles">
												<p>Upload Image</p>
											</div>
										</div>
									</div>

								</a>
								<input type="file" id="image_file" name="image" accept="image/*" style="display: none">
							</div>
							<div class ="col-md-3">
								<div id ="span_name" class ="form-group remove">
									<h4><span style="color:red;">*</span>Room name:</h4>
									<input type="text" class ="form-control room_name" name="name" value="{{!empty($info->name) ? $info->name : ''}}" style="text-transform: uppercase;">
									<label></label>
								</div>
							</div>
							<div class ="col-md-3">
								<div id ="span_available" class ="form-group remove">
									<h4>Room available:</h4>
									<input type="text" class ="form-control room_availabe" name="available"  value="{{!empty($info->available) ? $info->available : ''}}">
									<label></label>
								</div>
							</div>
							<div class ="col-md-3">
								<div id ="span_size" class ="form-group remove">
									<h4><span style="color:red;">*</span>Room size: <span style="color:red;"> +SQM. </span></h4>
									<input type="text" class ="form-control room_size" name="size" value="{{!empty($info->size) ? $info->size : ''}}">
									<label></label>
								</div>
							</div>
							<div class ="col-md-3">
								<div id ="span_capacity" class ="form-group remove">
									<h4><span style="color:red;">*</span>Room capacity:</h4>
									<input type="text" class ="form-control room_capacity" name="capacity"  value="{{!empty($info->capacity) ? $info->capacity : ''}}">
									<label></label>
								</div>
							</div>
							<div class ="col-md-3">
								<div id ="span_rate" class ="form-group remove">
									<h4><span style="color:red;">*</span>Room rate:</h4>
									<input type="text" class ="form-control room_rate" name="rate"  value="{{!empty($info->rate) ? $info->rate : ''}}">
									<label></label>
								</div>
							</div>
							<div class ="col-md-3">
								<div id ="span_bed" class ="form-group remove">
									<h4><span style="color:red;">*</span>Room Beds:</h4>
									<input type="text" class ="form-control room_bed" name="bed"  value="{{!empty($info->bed) ? $info->bed : ''}}">
									<label></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id ="span_description" class ="form-group remove">
									<h4><span style="color:red;">*</span>Room description:</h4>
									<textarea type="text" rows ="4"class ="form-control room_description" name="description"> {{!empty($info->description) ? $info->description : ''}}
									</textarea> 
									<label></label>
								</div>
							</div>
						</div>
						<div class="nav-tabs-custom">
                        	<ul class="nav nav-tabs">
                         		<li class ="active"><a href="#rooms" data-toggle="tab">ROOMS</a></li>
                     	 	</ul>
                     		<div class ="tab-content">
                        		<div class="active tab-pane" id="rooms">
                        			<div class ="row">
                        				<div class ="col-md-7">
                        					<table id ="roomTable" class="table table-bordered table-white">
					                            <thead>
					                                <tr class="table-light">
					                                	<th>Room no.</th>
					                                	<th><a href="javascript:;" class="btn btn-primary btn-flat add_row">
															<i class="fa fa-plus"></i> ADD ROW </a></th>
					                                </tr>
					                            </thead>
					                            <tbody>
					                            	@if(!empty($room_count))
					                            	@foreach($room_count as $rc)
					                                <tr>
														<td><input type ="text" class="form-control" placeholder="" name="room_no[]" value ="{{$rc->room_no}}">
														<input type="hidden" name="room_id[]" value="{{$rc->id}}"></td>
														<td><a href="javascript:;" class="btn btn-danger btn-flat delete_row">
															<i class="fa fa-trash"></i> REMOVE ROW </a></td>
					                                </tr>
					                                @endforeach
					                                @endif
					                            </tbody>
					                        </table>
                        				</div>
                        			</div>
                        		</div>
                        	</div>
                     	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-scripts')
<script type="text/javascript">
	$.ajaxSetup({
	  	headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	});
	// $('#btnSave').click(function(){
	// 	$('#frmSave').submit()
	// })
	$('#btnSave').on('click', function(){
		var id = "{!! !empty($info->id)? $info->id : '0' !!}";
        var input = $('#frmSave').serializeArray()
        var formData = new FormData()
        $.each(input,function(key,input){
       		formData.append(input.name,input.value);
    	});
    	var Filelist = $('#image_file')[0].files
    	formData.append('image', Filelist == 'undefined' ? '': Filelist[0]);
		 $.ajax({
		 	url: "{!! url('/room/save') !!}/" +id,
		 	type: "POST",
		 	dataType: "json",
		 	contentType: false,
        	processData: false,
        	cache: false,
        	data: formData,
        	success: function(e){
        		if(e.status == 1){
        			window.location = e.url
        		}
        		else{
        			$('.remove').find('label').html('')
        			$('.remove').removeClass('has-error')
        			$.each(e.errors,function(key, data){
        				if(data){
        					$('#span_'+key).find('label').html(data)
        					$('#span_'+key).addClass('has-error')
        				}
        			})
        		}
        	}
		 })
	})
	setTimeout(function() {
        $(".alert").slideUp(500, function(){
            $('.alert').css('display','none'); 
        });
    }, 1000);
    $('.profile-image').click(function(){
    	$('#image_file').trigger('click')
    })

    $('#image_file').change(function(){
	    var input = document.getElementById('image_file');
	    if(input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function(e) {
	            $('#image_preview').attr('src',e.target.result);
	     	}
	        reader.readAsDataURL(input.files[0]);
	    }
	});

	$(document).ready(function(){
		$('.visibles').attr('style','visibility: hidden !important;')
		$('.hover').mouseenter(function(){
			$(this).find('.visibles').attr('style','visibility: visible !important;')
		});
		$(".hover").mouseleave(function(){
			$(this).find('.visibles').attr('style','visibility: hidden !important;')
		});
	})

	$('.add_row').on('click',function(){
		addRow()
	})
	$(document).ready(function(){
		addRow()
	})
	function addRow(){
		var row = '<tr>'+
					'<td><input type ="text" class="form-control" placeholder="" name="room_no[]"></td>'+
					'<td>'+
                    '<a href="javascript:;" class="btn btn-danger btn-flat delete_row"><i class="fa fa-trash"></i> REMOVE ROW </a>'+
                	'</td>'+
                   '</tr>';
        $('#roomTable tbody').append(row)
        $('	td').delegate('.delete_row','click',function(){
        	$(this).parentsUntil('tbody').remove();
        })
	}



</script>

	
@stop

