@extends('admin.admin-layout.master-admin')
@section('page-css')
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<section class ="content">
	@include('alerts.alert-msg')
	<div class="row" id ="follow">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject text-uppercase"> View Rooms</span>
					</div>
					<div class="actions">
						<a href="{{url('/dashboard')}}" class="btn btn-default btn-flat">
							<i class="fa fa-arrow-left"></i> Back
						</a>
						<button href="#" class="btn btn-danger btn-flat" id="btnDelete">
							<i class="fa  fa-trash-o"></i> Delete
						</button>
						<a href="{{url('/room/form/new')}}" class="btn btn-success btn-flat">
							<i class="fa fa-edit"></i> Add New
						</a>
					</div>
				</div>
				<div class="portlet-body" style="padding: 0 15px">
					<form id="frmDelete" role="form">  {!! csrf_field() !!}
						<table id ="dataTable"  class="table table-hover">
                            <thead>
                                <tr class="table-light">
                                	<th> </th>
                                	<th>#</th>
                                	<th>Image</th>
                                	<th>Name</th>
                                	<th>Size</th>
                                	<th>Max Capacity</th>
                                	<th>Rate</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($rooms as $key => $r)
                                <tr class="table-light" id="{{$r->id}}">
                                	<td><span class ="align-middle"><input type ="checkbox" name="check_delete[]" class ="ids" value="{{$r->id}}"></span></td>
                                	<td class ="link"><span class ="align-middle">{{ $key + 1}}</span></td>
                                	<td class ="link"><img src="../../uploads/room_images/{{!empty($r->image) ? $r->image : 'not-available.jpg'}}" height="50" width="50"></td>
                                	<td class ="link"><span class ="align-middle">{{ $r->name}}</span></td>
                                	<td class ="link"><span class ="align-middle">{{ $r->size}} SQM.</span></td>
                                	<td class ="link"><span class ="align-middle">{{ $r->capacity}}</span></td>
                                	<td class ="link"><span class ="align-middle">{{ $r->rate}}</span></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-scripts')
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	$.ajaxSetup({
	  	headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	})
	$('#dataTable').DataTable()
	$('.link').click(function(){
		var id = $(this).closest('tr').attr('id')
		window.location = "/room/form/"+id
	})
	$('#btnDelete').off('click').on('click', function(){
		var confirms = confirm('Are you sure you want to Delete this?')
		if(confirms == true){
			if($('#frmDelete input[name="check_delete[]"]:checked').length > 0){
				var items = [];
    			$('.ids').each(function(){
      				if($(this).is(':checked')){
        				items.push($(this).val());
    				}
				});
				console.log(items)
				$.ajax({
					url: "/room/delete",
					method: "POST",
					dataType: "JSON",
					data:{
						id: items
					},
					success : function (e){
						if(e.status == 1){
							location.reload()
						}
					}
				})

			} else {
			alert('Please select atleast 1 item')
			}
		}

	})
	setTimeout(function() {
        $(".alert").slideUp(500, function(){
            $('.alert').css('display','none'); 
        });
    }, 1000);

</script>

	
@stop

