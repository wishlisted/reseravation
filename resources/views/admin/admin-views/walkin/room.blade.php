<table class="table table-striped table-bordered table-hover " id="reservation_table" style="pointer-events: none;">
	<thead>
		<th> Room Type </th>
		<th> Room Number </th>
		<th> Rate </th>
		<th> Breakfast</th>
		<th> <a href="javascript:;" class="btn btn-primary btn-flat add_row"><i class="fa fa-plus"></i> ADD ROW </a></th>
	</thead>
	<tbody>
		@if(!empty($rooms))
			@foreach($rooms as $key => $r)
				<tr>
					<td>
						<select class ="form-control room_type id_'+last_key+'" data-id = "'+last_key+'" name ="room_type[]">
							<option> {!!$r->name!!}</option>
						</select>
					</td>
					<td>
						<select class ="form-control room_number room_number_'+last_key+'" name ="room_id[]" data-id = "'+last_key+'">
							<option> {!!$r->room_no!!}</option>
						</select>
					</td>
					<td>
						<input type ="text" class ="form-control room_rate room_rate_'+last_key+'" name ="room_rate[]" data-id = "'+last_key+'" value="{!! $r->total_rate!!}" readonly>
					</td>
					<td>
						<!-- <label class="container1"><input type="checkbox" name="breakfast[]" value="1" data-id = "'+last_key+'" {!! $r->offer_id == 1 ? 'checked' : '' !!}><span class="checkmark"></span>Free Breakfast 
						</label> -->
						<select class ="form-control breakfast" name ="breakfast[]" ><option value="{!! $r->food_fee > 0 ? 1 : 0 !!}">{!! $r->food_fee > 0 ? "Yes" : "No" !!}</option></select>
					</td>
					<td><a href="javascript:;" class="btn btn-danger btn-flat delete_row" ><i class="fa fa-trash"></i> REMOVE </a></td>
                   </tr>
			@endforeach
		@endif

	</tbody>
</table>