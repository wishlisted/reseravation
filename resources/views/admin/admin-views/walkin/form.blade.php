@extends('admin.admin-layout.master-admin')
@section('page-css')
<style>
	.error{
		background-color: red !important;
	}
</style>

@stop

@section('content')
<section class ="content">
	@include('alerts.alert-msg')
	<div class="row" id ="follow">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject text-uppercase"> Walk-in Reservation</span>
					</div>
					<div class="actions">
						<a href="{{url('/walkin-reservation/index')}}" class="btn btn-default btn-flat">
							<i class="fa fa-arrow-left"></i> Back
						</a>
						<a href="{{url('/walkin-reservation/form/new')}}" class="btn btn-success btn-flat">
							<i class="fa fa-edit"></i> Add New
						</a>
						@if(empty($details))
						<button href="#" class="btn btn-primary btn-flat" id="btnSave">
							<i class="fa fa-save"></i> Save 
						</button>
						@endif
					</div>
				</div>
				<div class="portlet-body" style="padding: 0 15px">
					<div class="tabbable-custom">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_po" data-toggle="tab">
								Reservation Management </a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_po">
								<form id="frmSave" role="form">  {!! csrf_field() !!}
									<div class ="row">
										<div class ="col-md-2">
											<div id ="span_reference_number" class ="form-group remove">
												<h4><span style="color:red;">*</span>Reference Number</h4>
												<input type="text" class ="form-control room_name" name="reference_number" value="{!! $reference!!}" style="text-transform: uppercase;" readonly>
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_first_name" class ="form-group remove">
												<h4><span style="color:red;">*</span>First Name</h4>
												<input type="text" class ="form-control room_name" name="first_name" value="{!! !empty($details->first_name) ? $details->first_name : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_last_name" class ="form-group remove">
												<h4><span style="color:red;">*</span>Last Name</h4>
												<input type="text" class ="form-control room_name" name="last_name" value="{!! !empty($details->last_name) ? $details->last_name : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-6">
											<div id ="span_address" class ="form-group remove">
												<h4>Address</h4>
												<input type="text" class ="form-control room_name" name="address" value="{!! !empty($details->address) ? $details->address : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_contact_number" class ="form-group remove">
												<h4><span style="color:red;">*</span>Contact Number</h4>
												<input type="text" class ="form-control room_name" name="contact_number" value="{!! !empty($details->contact_number) ? $details->contact_number : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_guest_number" class ="form-group remove">
												<h4><span style="color:red;">*</span>Guest Number</h4>
												<input type="text" class ="form-control room_name" name="guest_number" value="{!! !empty($details->guest_number) ? $details->guest_number : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_date_from" class ="form-group remove">
												<h4><span style="color:red;">*</span>Date From</h4>
												<input type="text" class ="form-control room_name" id ="checkin_date" name="date_from" value="{!! !empty($details->date_from) ? $details->date_from : ''!!}" style="text-transform: uppercase; pointer-events: {!! !empty($details->date_from) ? 'none' : ''!!}">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_date_to" class ="form-group remove">
												<h4><span style="color:red;">*</span>Date To</h4>
												<input type="text" class ="form-control room_name" id ="checkout_date" name="date_to" value="{!! !empty($details->date_to) ? $details->date_to : ''!!}" style="text-transform: uppercase; pointer-events: {!! !empty($details->date_to) ? 'none' : ''!!}">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_payment_method" class ="form-group remove">
												<h4><span style="color:red;">*</span>Payment Method</h4>
												<select class ="form-control room_name" name="payment_method" value="" style="text-transform: uppercase;">
													<option value="1">Cash on hand</option>
												</select>
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>total amount</h4>
												<input type="text" class ="form-control total" name="total" value="{!! !empty($details->total) ? $details->total : ''!!}" style="text-transform: uppercase;" readonly="">
												<label></label>
											</div>
										</div>

										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Function Room</h4>
												<input type="number" class ="form-control additional" name="function_room" value="{!! !empty($details->function_room) ? $details->function_room : ''!!}" style="text-transform: uppercase;" >
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Videorent</h4>
												<input type="number" class ="form-control additional" name="videorent" value="{!! !empty($details->videorent) ? $details->videorent : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Bon fire</h4>
												<input type="number" class ="form-control additional" name="bon_fire" value="{!! !empty($details->bonfire) ? $details->bonfire : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Bananaboat</h4>
												<input type="number" class ="form-control additional" name="bananaboat" value="{!! !empty($details->bananaboat) ? $details->bananaboat : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Corckage</h4>
												<input type="number" class ="form-control additional" name="corckage" value="{!! !empty($details->corckage) ? $details->corckage : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Breakfasttotal</h4>
												<input type="number" class ="form-control additional" name="breakfasttotal" value="{!! !empty($details->total_breakfast) ? $details->total_breakfast : ''!!}" style="text-transform: uppercase;" readonly>
												<label></label>
											</div>
										</div>
										<div class ="col-md-2">
											<div id ="span_total" class ="form-group remove">
												<h4>Bar</h4>
												<input type="number" class ="form-control additional" name="bar" value="{!! !empty($details->bar) ? $details->bar : ''!!}" style="text-transform: uppercase;">
												<label></label>
											</div>
										</div>
									</div>
									<hr>
									<div class ="row">
										<div class ="col-md-12">
											@include('admin.admin-views.walkin.room')
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-scripts')
<script type="text/javascript">
	$.ajaxSetup({
	  	headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	});


	$(document).ready(function(){
		// $('.additional').attr('disabled','true')
		addRow(0) 	
	})
	var date_interval
	var check_key = 1
	var date = new Date();
	var food_fee = parseInt("{!! $food_fee !!}")
	$('#checkin_date').datepicker({
		autoclose: true,
     	format: 'yyyy-mm-dd',
     	startDate: date,
	})
	$('#checkout_date').datepicker({
		autoclose: true,
     	format: 'yyyy-mm-dd',
     	startDate: date,
	})
	$('body').delegate('.breakfast', 'change', function(){
		var value = $(this).val()

		if(value == 1){
			$('.total').val(super_total + (food_fee * date_interval))
			super_total = super_total + (food_fee * date_interval)
			// totalRate(food_fee, date_interval)
			// var total = 0
			// $('.room_rate').each(function (index, element) {
	  //       	total = total + parseFloat($(element).val());
	  //   	});

	    	// $('.total').val((total*date_interval) + food_fee)
		}
		else{
			$('.total').val(super_total - (food_fee * date_interval));
			super_total = super_total - (food_fee * date_interval);
			// var total = 0
			// $('.room_rate').each(function (index, element) {
	  //       	total = total + parseFloat($(element).val());
	  //   	});

	  //   	$('.total').val((total*date_interval))
			// totalRate(-((food_fee/1)), date_interval)
		}
	})
	$('#btnSave').on('click', function(){
		var data_ids = [];
		var checker = [];
		$('.room_number').each(function (index, element) {
			checker[$(element).data('id')] = $(element).val();
    	});

    	var sorted_arr = checker.slice().sort(); 
		var results = [];
		for (var i = 0; i < sorted_arr.length - 1; i++) {
		    if (sorted_arr[i + 1] == sorted_arr[i]) {
		        results.push(sorted_arr[i]);
		    }
		}
		console.log(super_total,data_ids.length,results.length,checker)
		if (results.length > 0 && data_ids.length > 1) {
			$(checker).each(function (index, element) {
				$(results).each(function (index1, element1) {
					if (element == element1) {
						data_ids.push(index);
					}
		    	});
	    	});

			$(checker).each(function (index1, element1) {
				if($('.room_type').data('id') == index1){
					var r_number = '.room_number_'+index1;

					$(r_number).closest("tr").addClass('error');
				}
	    	});

		}else{
			var id = "{!! !empty($info->id)? $info->id : '0' !!}";
	        var input = $('#frmSave').serializeArray()
	        var formData = new FormData()
	        $.each(input,function(key,input){
	       		formData.append(input.name,input.value);
	    	});
			 $.ajax({
			 	url: "{!! url('/walkin-reservation/save') !!}/" +id,
			 	type: "POST",
			 	dataType: "json",
			 	contentType: false,
	        	processData: false,
	        	cache: false,
	        	data: formData,
	        	success: function(e){
	        		console.log(e)
	        		if(e.status == 1){
	        			window.location = e.url
	        		}
	        		else{
	        			$('.remove').find('label').html('')
	        			$('.remove').removeClass('has-error')
	        			$.each(e.errors,function(key, data){
	        				if(data){
	        					$('#span_'+key).find('label').html(data)
	        					$('#span_'+key).addClass('has-error')
	        				}
	        			})
	        		}
	        	}
			 })
		}
		
	})
	setTimeout(function() {
        $(".alert").slideUp(500, function(){
            $('.alert').css('display','none'); 
        });
    }, 1000);
    $('.profile-image').click(function(){
    	$('#image_file').trigger('click')
    })

    $('#image_file').change(function(){
	    var input = document.getElementById('image_file');
	    if(input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function(e) {
	            $('#image_preview').attr('src',e.target.result);
	     	}
	        reader.readAsDataURL(input.files[0]);
	    }
	});
	function showDays(firstDate,secondDate){
                

	var startDay = new Date(firstDate);
	var endDay = new Date(secondDate);
	var millisecondsPerDay = 1000 * 60 * 60 * 24;

	var millisBetween = startDay.getTime() - endDay.getTime();
	var days = millisBetween / millisecondsPerDay;
	days = (Math.floor(days) < 1) ? 1 : Math.floor(days)

	return days

    }
	function addRow(last_key){
		var last_key = last_key + 1
		var row = '<tr>'+
					'<td><select class ="form-control room_type id_'+last_key+'" data-id = "'+last_key+'" name ="room_type[]"></select></td>'+
					'<td><select class ="form-control room_number room_number_'+last_key+'" name ="room_id[]" data-id = "'+last_key+'"></select></td>'+
					'<td><input type ="text" class ="form-control room_rate room_rate_'+last_key+'" name ="room_rate[]" data-id = "'+last_key+'" readonly></td>'+
					'<td><input type="number" class="form-control breakfast breakfast_'+last_key+'" data-id = "'+last_key+'" name ="breakfast[]"  data-id = "'+last_key+'"></td>'+
					'<td><a href="javascript:;" class="btn btn-danger btn-flat delete_row" ><i class="fa fa-trash"></i> REMOVE </a></td>'+
                   '</tr>';
        $('#reservation_table tbody').append(row)
		$.ajax({
        	url: "/walkin-reservation/room-type",
		 	type: "get",
		 	dataType: "json",	
		 	success : function(e){
		 		select = '<option> select room type</option>'
		 		$.each(e.data, function(key, value){
		 			select += '<option value ='+value.id+'>'+value.name+'</option>'
		 		})
		 		$('.id_'+last_key+'').html(select)
		 	}
        })
        check_key = last_key;
	}
	var super_total = 0;
	function totalRate(date_interval){
		var function_room = parseFloat($('input[name="function_room"]').val() ? $('input[name="function_room"]').val() : 0 );
		var videorent = parseFloat($('input[name="videorent"]').val() ? $('input[name="videorent"]').val() : 0 );
		var bon_fire = parseFloat($('input[name="bon_fire"]').val() ? $('input[name="bon_fire"]').val() : 0 );
		var bananaboat = parseFloat($('input[name="bananaboat"]').val() ? $('input[name="bananaboat"]').val() : 0 );
		var corckage = parseFloat($('input[name="corckage"]').val() ? $('input[name="corckage"]').val() : 0 );
		var bar = parseFloat($('input[name="bar"]').val() ? $('input[name="bar"]').val() : 0 );

		var t_breakfast = 0;
		$('.breakfast').each(function (index, element) {
			t_breakfast += (parseFloat($(element).val())? parseFloat($(element).val()) : 0);
    	});

		var total = 0
		$('.room_rate').each(function (index, element) {
        	// total = total + parseFloat($(element).val());
        	total += (parseFloat($(element).val()) ? parseFloat($(element).val()) : 0 );
    	});
		super_total = ( (total*date_interval) + (date_interval * t_breakfast) + function_room + videorent + bon_fire + bananaboat + corckage + bar);

		// console.log(total,date_interval,t_breakfast,function_room,videorent,bon_fire,bananaboat,corckage,bar);
    	$('.total').val(super_total)		

	}

	function totalBreakfast(date_interval){
		var t_breakfast = 0;
		$('.breakfast').each(function (index, element) {
			t_breakfast += (parseFloat($(element).val())? parseFloat($(element).val()) : 0);
    	});

    	$('input[name="breakfasttotal"]').val(date_interval * t_breakfast)		

	}

	$('body').delegate('.room_type','change',function(){
		var this_key = $(this).data('id')
		var this_id = $(this).val()
		totalRate()
		totalBreakfast(date_interval);

        $.ajax({
        	url: '/walkin-reservation/room-number',
        	type: "GET",
        	data : {
        		id : $(this).val(),
        		date_from : $('#checkin_date').val(),
        		date_to : $('#checkout_date').val()
        	},
        	dateType: "json",
        	success : function(e){
        		room_select =''
        		$.each(e.data, function(key, value){
		 			room_select += '<option value ='+value.id+'>'+value.room_no+'</option>'
		 		})
		 		$('.room_number_'+this_key+'').html(room_select)
        	}
        })

        $.ajax({
        	url: '/walkin-reservation/rate',
        	type: "GET",
        	data : {
        		id : this_id,
        	},
        	dateType: "json",
        	success : function(e){
        		$('.room_rate_'+this_key+'').val(e.rate)
        		console.log(totalRate(date_interval))
        		
        	}
        })

    })
    $('body').delegate('.breakfast','keyup',function(){
		// var this_key = $(this).data('id')
		// var this_id = $(this).val()
		var this_key = $(this).closest(".room_type").data('id')
		var this_id = $(this).closest(".room_type").val()

    	$.ajax({
        	url: '/walkin-reservation/rate',
        	type: "GET",
        	data : {
        		id : this_id,
        	},
        	dateType: "json",
        	success : function(e){
        		$('.room_rate_'+this_key+'').val(e.rate)
        		console.log(totalRate(date_interval))
        		
        	}
        })
    	totalBreakfast(date_interval);
    })
    $('body').delegate('.additional','keyup',function(){
		totalRate(date_interval)
    	totalBreakfast(date_interval);
    })
    $('body').delegate('.add_row','click',function(){
    	addRow(check_key);
    })
    $('body').delegate('.delete_row','click',function(){
    	if($('.room_type').length > 1){
    		$(this).parentsUntil('tbody').remove();	
    	}
    })

    $('#checkin_date').on('change',function(){
    	if(!($(this).val()) || !($('#checkout_date').val())){
    		$('#reservation_table').css('pointer-events','none')	
    		// $('.additional').attr('disabled','disabled').remove()
    	}
    	else{
    		$('#reservation_table').css('pointer-events','auto')
    		// $('.additional').attr('disabled','disabled').remove()
    		var checkin_date = $(this).val()
    		var checkout_date  = $('#checkout_date').val()
    		date_interval = showDays(checkout_date, checkin_date)

    	}

    })
    $('#checkout_date').on('change',function(){
    	if(!($(this).val()) || !($('#checkin_date').val())){
    		$('#reservation_table').css('pointer-events','none')
    		// $('.additional').attr('disabled','disabled').remove()
    	}
    	else{
    		$('#reservation_table').css('pointer-events','auto')
    		// $('.additional').attr('disabled','disabled').remove()
    		var checkin_date = $('#checkin_date').val()
    		var checkout_date  = $(this).val()
    		date_interval = showDays(checkout_date, checkin_date)
    	}
    })


</script>

	
@stop

