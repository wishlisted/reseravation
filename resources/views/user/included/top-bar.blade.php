<div class="container clearfix">
	<div class="contact-info float-left user-info user">
		<a href ="" class ="login-user" data-toggle="modal" data-target="#user-modal">Sign in Now</a>
		
	</div>
	<div class="social-links float-right use-info" style="margin-top: 3px">
		<a href ="#" style="line-height: 20px;"> <i class="fa fa-map-marker"> </i> Juan Luna St. Poblacion Morong, Bataan</a>
		<a  href ="#" style="line-height: 20px;"><i class="fa fa-phone"> </i> +63912345678</a>
	</div>
</div>

<div class="modal fade" id="user-modal">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="control-label" style="text-align: left;">Login</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					@include('alerts.alert-msg')
					<form id="frmLogin">
						<div class ="row">
							<div class ="col-md-12">
								<div id="span_username" class="form-group remove">
									<input type="text" name="username" id="username" class="form-control" placeholder="Enter Username">
									<label></label>
								</div>
							</div>
							<div class ="col-md-12">
								<div id="span_password" class="form-group remove">
									<input type="password" name="password" id="password" class ="form-control" placeholder="Enter Password">
									<label></label>
								</div>
							</div>
							<div class ="col-md-12" style="position: relative;">
								<div class="col-md-6" style="padding: 0px;">
									<a href="{{url('/signup')}}" id="btn-register" class="btn btn-default">Register</a>
								</div>
								<div class="col-md-6" style="padding: 0px;">
									<a href ="javascript:;" class ="btn btn-default" id ="btn-login" style=" position: absolute; right: 0;">Login</a>
								</div>
								<!-- <div class="col-md-6">
									<button class ="btn btn-default"> Back </button>
								</div> -->
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>