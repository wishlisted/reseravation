<div class="container">
	<div id="logo" class="pull-left"></div>
	<nav id="nav-menu-container">
		<ul class="nav-menu">
			<li class="{{ Request::segment(1) == ''? 'menu-active' : '' }}"><a href="{{url('/')}}">Home</a></li>
			<li class="{{ Request::segment(1) == 'rooms-availability'? 'menu-active' : '' }}"
			><a href="{{url('/rooms')}}">Rooms</a></li>
			<li><a href="#about">About Us</a></li>	
			<li><a href="#contact">Contact us</a></li>
	
		</ul>
	</nav>
</div>