<div class="container">		
	<hr>
	<div class="copyright">
		<div class ="row">
			<div class ="col-md-4"> 
				<h4>CONTACT US</h4>
				<p><i class ="fa fa-map-marker"></i> Juan Luna St. Poblacion Morong, Bataan</p>
				<p><i class ="fa fa-phone"></i> +63912345678</p>
				<p><i class ="fa fa-envelope"></i> sea&river@gmail.com</p>
			</div>
			<div class ="col-md-4"> 
				<h4>SEA & RIVER VIEW</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in sollicitudin erat, vitae posuere lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus. diam commodo. Sed convallis te ipsum primis in </p>
			</div>
			<div class ="col-md-4" style="">
				<h4 style="margin-bottom: 30px;">SOCIAL MEDIA'S</h4>
				<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
				<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
				<a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
				<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
				<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
			</div>
		</div>
	</div>
	<hr>
	<div class ="rights" style="text-align: center; margin-top: 20px;">
		<p style="letter-spacing: 1px; font-weight: bold; color: black">© ALL RIGHTS RESERVED SEA & RIVER VIEW </p>
	</div>
</div>
