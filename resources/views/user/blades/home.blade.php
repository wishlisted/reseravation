@extends('user.layouts.master-user')
@section('page-css')
 

@stop

@section('content')
	<section id="intro">
		<div class ="introcontent">
			<div class="container">
				<div id="logo" class="pull-left"></div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li class="menu-active"><a href="{{url('/')}}">Home</a></li>
						<li><a href="{{url('/rooms')}}">rooms</a></li>
						<li><a href="#about">About Us</a></li>	
						<li><a href="#contact">Contact us</a></li>
					</ul>
				</nav>
			</div>
			<!-- <div class="container">
				<h2>We bring you the <span>BEST.</span><a href="#about" class="btn-get-started scrollto">BOOK NOW!</a></h2>		
			</div> -->
		</div>
		<div id="intro-carousel" class="owl-carousel">
			<div class="item" style="background-image: url('template/img/intro-carousel/1.jpg');"></div>
			<div class="item" style="background-image: url('template/img/intro-carousel/2.jpg');"></div>
			<div class="item" style="background-image: url('template/img/intro-carousel/3.jpg');"></div>
		</div>
	</section><!-- #intro -->

	<section id="about" class="wow fadeInUp reserve">
		<div class="container">
			<form id="frmRoom" role="form">
				<div class="row">
					<div class ="col-md-4">
						<div class ="room">
							<h4> Checkin: </h4>
							<div id="span_checkin" class="form-group remove">
	                			<div class="input-group date">	                		
	                  				<div class="input-group-addon">
	                    				<i class="fa fa-calendar-check-o"></i>
	                  				</div>
	                 				 <input type="text" class="form-control pull-right" id="checkin_date" name ="checkin">
	                			</div>
	                			<label></label>
	              			</div>
						</div>
					</div>
					<div class ="col-md-4">
						<div class ="room">
							<h4> Checkout: </h4>
							<div id="span_checkout" class="form-group remove">
	                			<div class="input-group date">
	                  				<div class="input-group-addon">
	                    				<i class="fa fa-calendar-times-o"></i>
	                  				</div>
	                 				 <input type="text" class="form-control pull-right" id="checkout_date" name="checkout">
	                			</div>
	                			<label></label>
	              			</div>
						</div>
					</div>
					<div class ="col-md-2">
						<div class ="room">
							<h4> Rooms: </h4>
							<div id="span_rooms" class="form-group remove">
	                			<div class="input-group date">
	                  				<div class="input-group-addon">
	                    				<i class="fa  fa-bed"></i>
	                  				</div>
	                 				  <input type="number" min ="1" max ="30" class="form-control pull-right" id="room_no" name="rooms">
	                			</div>
	                			<label></label>
	              			</div>
						</div>
					</div>
					<div class ="col-md-2">
						<div class ="room">
							<a href = "javascript:;" class="btn" id ="btn-book"> Check Availability</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section><!-- #about -->

	<section id="portfolio" class="wow fadeInLeft head about_rooms">
		<div class="container">
			<div class ="col-md-12">
				<div class ="title">
					<p>Conscious hospitality</p>
					<h1>ROOMS & SUITES</h1>
					<hr>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">	
				<div class="col-md-4">	
					<div class="portfolio-item wow fadeInUp hover">
						<a href=#" class="">
							<img src="template/img/portfolio/1.jpg" alt="">
							<div class="portfolio-overlay">
								<div class="portfolio-info ">
									<div class="visibles" style="visibility: hidden !important;">
										<h2>Standard Room</h2>
										<p class ="price"> <span class="peso"> ₱2800.00</span>/Night </p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="infos">
						<div class ="infos_description">
							<div class ="infos_title">
								<h3>STANDARD ROOM</h3>
								<hr>
							</div>
							<h4>DETAILS:</h4>
							<div class="row" style="margin-bottom: 20px;">
								<div class ="col-md-4">
									<h5>SIZE: <span class ="infos_inner">17 SQM. </span></h5>
								</div>
								<div class ="col-md-4">
									<h5>MAX PAX: <span class ="infos_inner">3</span></h5>
								</div>
								<div class ="col-md-4">
									<h5>BEDs: <span class ="infos_inner"> 1</span></h5>
								</div>
							</div>
							<div class ="infos_laman"">
								<h4  style="margin-bottom: 5px;">DESCRIPTION:</h4>
								<p>	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the typesetting industry. Lorem Ipsum has been the industry's.</p>
							</div>
							<div class="more">
								<a href="#">VIEW MORE >></a>
							</div>
							
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="portfolio-item wow fadeInUp hover">
						<a href=#" class="">
							<img src="template/img/portfolio/2.jpg" alt="">
							<div class="portfolio-overlay">
								<div class="portfolio-info ">
									<div class="visibles" style="visibility: hidden !important;">
										<h2>Executive Room</h2>
										<p class ="price"> <span class="peso"> ₱3920.00</span>/Night </p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="infos">
						<div class ="infos_description">
							<div class ="infos_title">
								<h3>EXECUTIVE ROOM</h3>
								<hr>
							</div>
							<h4>DETAILS:</h4>
							<div class="row" style="margin-bottom: 20px;">
								<div class ="col-md-4">
									<h5>SIZE: <span class ="infos_inner">20 SQM. </span></h5>
								</div>
								<div class ="col-md-4">
									<h5>MAX PAX: <span class ="infos_inner">4</span></h5>
								</div>
								<div class ="col-md-4">
									<h5>BEDs: <span class ="infos_inner">2</span></h5>
								</div>
							</div>
							<div class ="infos_laman"">
								<h4  style="margin-bottom: 5px;">DESCRIPTION:</h4>
								<p>	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the typesetting industry. Lorem Ipsum has been the industry's.</p>
							</div>
							<div class="more">
								<a href="#">VIEW MORE >></a>
							</div>
							
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="portfolio-item wow fadeInUp hover">
						<a href=#" class="">
							<img src="template/img/portfolio/3.jpg" alt="">
							<div class="portfolio-overlay">
								<div class="portfolio-info ">
									<div class="visibles" style="visibility: hidden !important;">
										<h2>deluxe Room</h2>
										<p class ="price"> <span class="peso"> ₱4760.00</span>/Night </p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="infos">
						<div class ="infos_description">
							<div class ="infos_title">
								<h3>deluxe ROOM</h3>
								<hr>
							</div>
							<h4>DETAILS:</h4>
							<div class="row" style="margin-bottom: 20px;">
								<div class ="col-md-4">
									<h5>SIZE: <span class ="infos_inner">25 SQM. </span></h5>
								</div>
								<div class ="col-md-4">
									<h5>MAX PAX: <span class ="infos_inner">5</span></h5>
								</div>
								<div class ="col-md-4">
									<h5>BEDs: <span class ="infos_inner">3</span></h5>
								</div>
							</div>
							<div class ="infos_laman"">
								<h4  style="margin-bottom: 5px;">DESCRIPTION:</h4>
								<p>	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the typesetting industry. Lorem Ipsum has been the industry's.</p>
							</div>
							<div class="more">
								<a href="#">VIEW MORE >></a>
							</div>
							
						</div>
					</div>
				</div>

				<!-- <div class="col-lg-4 col-md-offset-2 col-md-4">
					<div class="portfolio-item wow fadeInUp hover">
						<a href=#" class="">
							<img src="template/img/portfolio/4.jpg" alt="">
							<div class="portfolio-overlay">
								<div class="portfolio-info ">
									<div class="visibles" style="visibility: hidden !important;">
										<h2>VIP ROOM</h2>
											<p class ="price"> <span class="peso"> ₱5320.00</span>/Night </p>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div> -->

			</div>
		</div>
			<!-- 		<div class="container">
			<div class ="col-md-12">
				<div class ="title bottom">
					<p><i class ="fa fa-angle-double-down"></i></p>
					<a href ="#" class ="book"> VIEW MORE ROOMS >></a>
				</div>
			</div>
		</div> -->
	</section>

	<section id ="what" class="wow fadeInDown">
		<div class="container">
			<div class ="col-md-12">
				<div class ="title">
					<h1>WELCOME TO SEA & RIVER VIEW</h1>
					<hr>
				</div>
			</div>
			<div class="col-md-12">
				<div class ="info">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in sollicitudin erat, vitae posuere lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus.  diam commodo. Sed convallis orci ut neque condimentum tincidunt. Morbi Ut erat lacus, porttitor a augue vitae, tincidunt sodales metus. Sed mollis vulputate nibh, quis pharetra arcu sollicitudin ut. Phasellus vehicula purus eget urna auctor, non efficitur diam commodo. Sed convallis orci ut neque condimentum tincidunt. Morbi lobortis ac leo vitae bibendum. Nam placerat mattis facilisis.</p>
				</div>
			</div>
		</div>	
	</section>

	<section id="portfolio" class="wow fadeInUp galleries">
		<div class="container">
			<div class ="col-md-12">
				<div class ="title">
					<p>Accommodation</p>
					<h1>GALLERIES</h1>
					<hr>
				</div>
			</div>
		</div>
      	<div class="container-fluid">
        	<div class="row no-gutters">
          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/1.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/1.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/2.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/3.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/3.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/3.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/4.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/4.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/5.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/5.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/6.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/6.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/7.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/7.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>

          		<div class="col-lg-3 col-md-4">
            		<div class="portfolio-item wow fadeInUp">
              			<a href="template/img/portfolio/8.jpg" class="portfolio-popup">
                			<img src="template/img/portfolio/8.jpg" alt="">
                			<div class="portfolio-overlay">
                  				<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>
                			</div>
              			</a>
            		</div>
          		</div>
        	</div>
      	</div>
    </section><!-- #portfolio -->

    <section id ="amenities" class ="wow fadeInRight does">
    	<div class="container">
				<div class ="col-md-6">
					<div class ="title">
						<p>All our rooms come with great amenities and services. The following are just a few of the main amenities included by default with our rooms.</p>
					</div>
				</div>
			</div>
    	<div class="container-fluid">
    		<div class="icons">
    			<div class ="col-md-12">
    				<div class ="container" style="">
    					<div class="row">
    						<div class ="col-md-4">
    							<!-- <img style="float:left; margin-right: 10px;" src="/template/img/amenities/icon-aircon.png" width="80" height="80">
    							<p style="padding-top:15px;">Fully Airconditioned</p> -->
    						</div>
    						<div class ="col-md-4">
    							<!-- <img style="float:left; margin-right: 10px;" src="/template/img/amenities/icon-aircon.png" width="80" height="80">
    							<p style="padding-top:15px;">Fully Airconditioned</p> -->
    						</div>
    						<div class ="col-md-4">
    	<!-- 							<img style="float:left; margin-right: 10px;" src="/template/img/amenities/icon-aircon.png" width="80" height="80">
    							<p style="padding-top:15px;">Fully Airconditioned</p> -->
    						</div>
    					</div>

    				</div>
    			</div>
    		</div>
    	</div>
    </section>
@stop

@section('page-scripts')
	
	<script type="text/javascript">
		$.ajaxSetup({
	  		headers: {
	    		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  		}
		});
		var date = new Date();
		date.setDate(date.getDate());
		$('#checkin_date').datepicker({
     		autoclose: true,
     		format: 'yyyy-mm-dd',
     		startDate: date,
     		onSelect: function(e){
     			
     		}
    	}).on('change', function(){
    		var myDate = $(this).datepicker('getDate');
    		myDate.setDate(myDate.getDate()+1); 
    		$("#checkout_date").datepicker("setDate", myDate)
    		$("#checkout_date").datepicker("setStartDate", myDate)
    	})

    	$('#checkout_date').datepicker({
     		autoclose: true,
     		format: 'yyyy-mm-dd',
    	})
    	

		$(document).ready(function(){
			$('.visibles').attr('style','visibility: hidden !important;')
			$('.hover').mouseenter(function(){
				$(this).find('.visibles').attr('style','visibility: visible !important;')
			});
			$(".hover").mouseleave(function(){
				$(this).find('.visibles').attr('style','visibility: hidden !important;')
			});
		})

		$('#btn-book').on('click', function(){
			var input = $('#frmRoom').serializeArray()
			var formData = new FormData()
			$.each(input, function(key, input){
				formData.append(input.name,input.value)
			})
			$.ajax({
				url: '/check-availability',
				type: 'POST',
				dataType: 'json',
				contentType: false,
	        	processData: false,
	        	cache: false,
	        	data: formData,
	        	success:function(e){
	        		if(e.status == 0){
	        			$('.remove').find('label').html('')
	        			$('.remove').removeClass('has-error')
	        			$.each(e.errors,function(key, data){
	        				if(data){
	        					$('#span_'+key).find('label').html(data)
	        					$('#span_'+key).addClass('has-error')
	        				}
	        			})
	        		} else {
	        			localStorage.setItem('checkin', $('#checkin_date').val())
	        			localStorage.setItem('checkout', $('#checkout_date').val())
	        			localStorage.setItem('room_count', $('#room_no').val() + 'Room/s')
	        			window.location = "/rooms"
	        		}
	        	}
	        })
		})
		
	</script>
@stop

