@extends('user.layouts.master-user')
@section('page-css')

@stop

@section('content')
	<div class="container">
		<div id="logo" class="pull-left"></div>
		<nav id="nav-menu-container">
			<ul class="nav-menu">
				<li><a href="{{url('/')}}">Home</a></li>
				<li class="menu-active"><a href="{{url('/rooms')}}">rooms</a></li>
				<li><a href="#about">About Us</a></li>	
				<li><a href="#contact">Contact us</a></li>

			</ul>
		</nav>
	</div>
	<section id="room_book" class ="book_room">
		<div class="container">
			<form id="frmRoom" role="form">
				<div class="row">
					<div class ="col-md-3">
						<div class ="room">
							<h4> Checkin: </h4>
							<div id="span_checkin" class="form-group remove">
	                			<div class="input-group date">	                		
	                  				<div class="input-group-addon">
	                    				<i class="fa fa-calendar-check-o"></i>
	                  				</div>
	                 				 <input type="text" class="form-control pull-right" id="checkin_date" name ="checkin">
	                			</div>
	                			<label></label>
	              			</div>
						</div>
					</div>
					<div class ="col-md-3">
						<div class ="room">
							<h4> Checkout: </h4>
							<div id="span_checkout" class="form-group remove">
	                			<div class="input-group date">
	                  				<div class="input-group-addon">
	                    				<i class="fa fa-calendar-times-o"></i>
	                  				</div>
	                 				 <input type="text" class="form-control pull-right" id="checkout_date" name="checkout">
	                			</div>
	                			<label></label>
	              			</div>
						</div>
					</div>
					<div class ="col-md-3">
						<div class ="room_include">
							<h4> Rooms: </h4>
							<div id="span_rooms" class="form-group remove">
		                			<div class="input-group date">
		                  				<div class="input-group-addon">
		                    				<i class="fa  fa-bed"></i>
		                  				</div>
		                 				 <a href ="#" class="btn form-control" id ="btn-rooms_count" data-toggle="modal" data-target="#room_info"> SELECT ROOM/s </a>
		                			</div>
	                			<label></label>
	              			</div>
						</div>
					</div>
					<div class ="col-md-3">
						<div class ="room">
							<a href = "javascript:;" class="btn form-control btn-book" id =""> Book Now!</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
	<section id="room_bed" class ="bed_room">
		<div class="container">
			<div class="row">
				<div class ="col-md-3 room_filters">
				</div>
				<div class ="col-md-9 room_view">	
					<div class="row">
						@foreach($rooms as $key => $r)
						<div class ="col-md-12" style="margin-bottom: 20px;">
							<div class ="col-md-4">
								<img src="../../uploads/room_images/{{!empty($r->image) ? $r->image : 'not-available.jpg'}}" width="100%" height="220" style=""> 
							</div>
							<div class ="col-md-8">
								<div class ="room_information">
									<div class ="info_header">
										<h3> {{ $r->name}} ROOM <span class="peso"> {{ $r->rate}}/<span style ="text-transform:lowercase">night</span> </span>
										</div>
									<div class ="info_body">
										<div class ="row"  style="margin-left: 15px;"> 
											<div class="col-md-4">
												<h5>SIZE: <span class="infos_inner">{{ $r->size}} SQM. </span></h5>
											</div>
											<div class="col-md-3">
												<h5>MAX PAX: <span class="infos_inner">{{ $r->capacity}} </span></h5>
											</div>
											<div class="col-md-3">
												<h5>BED/s: <span class="infos_inner">{{ $r->bed}} </span></h5>
											</div>
										</div>
										<div class="row" style="margin-top:5px;"> 
											<div class="col-md-12">
												<h4>DESCRIPTION: </h4>
												<p>{{ $r->description}}</p>
											</div>
										</div>
										<hr>
									</div>

									<div class ="info_footer">
										<button class ="btn add_room" value="{{$r->id}}">ADD ROOM</button>
									</div>
								</div>
							</div>

						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="room_info">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<table id ="roomTable" class="table">
							<thead>
								<tr class="table-light">
									<th></th>
									<th>type</th>
									<th>rooms</th>
									<th>rate</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	@include('user.blades.modals.modal')
@stop

@section('page-scripts')
	<script type="text/javascript">
		$.ajaxSetup({
	  		headers: {
	    		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  		}
		});
		var room_count = localStorage.getItem("room_count")
		var checkin = localStorage.getItem("checkin")
		var checkout = localStorage.getItem("checkout")
		var night_duration
		$('#checkin_date').val(checkin ? checkin : '')
		$('#checkout_date').val(checkout ? checkout : '')
		var date = new Date();
		date.setDate(date.getDate());
		$('#checkin_date').datepicker({
     		autoclose: true,
     		format: 'yyyy-mm-dd',
     		startDate: date,
     		onSelect: function(e){
     		}
    	}).on('change', function(){
    		var myDate = $(this).datepicker('getDate');
    		myDate.setDate(myDate.getDate()+1); 
    		// console.log(myDate)
    		
    		$("#checkout_date").datepicker("setStartDate", myDate)
    		$("#checkout_date").datepicker("setDate", myDate)
    		localStorage.setItem('checkin', $('#checkin_date').val())
	        localStorage.setItem('checkout', $('#checkout_date').val())
	        
    	})
    	$('#checkout_date').datepicker({
     		autoclose: true,
     		format: 'yyyy-mm-dd',
     		startDate: date,
     		onSelect: function(e){
     		}
    	}).on('change', function(){
	        localStorage.setItem('checkout', $('#checkout_date').val())
	        var start =  new Date($('#checkin_date').val())
			var end = new Date ($('#checkout_date').val())
			var diff = new Date(end - start);
			night_duration =  diff/1000/60/60/24;
			da = localStorage.setItem('night_duration', night_duration)
			console.log(localStorage.getItem('night_duration'))

    	})

    	$('#checkout_date').datepicker({
     		autoclose: true,
     		format: 'yyyy-mm-dd',
    	})

    	
    	$(document).ready(function(){
    		var added_room = JSON.parse(localStorage.getItem('rooms'))
    		if(added_room == null){
    			var session_rooms = {}
    			session_rooms[0] = {
    				'id' : '',
					'quantity': ''
    			}
				localStorage.setItem('rooms', JSON.stringify(session_rooms))
    		}		
    	})
		$('.add_room').click(function(){
			var id = $(this).val()
			var session_rooms = JSON.parse(localStorage.getItem('rooms'))
			var exist_key = session_rooms[id]

			$.each(session_rooms, function(key, data){
				// console.log(key+ '|' + id)
				if(key == id){
					console.log(key)
						session_rooms[key].quantity =  parseInt(session_rooms[key].quantity) + 1
						console.log(session_rooms[key].quantity)
				}	
				else{
					if(exist_key == null){
						session_rooms[id] = {
							'id' : id,
							'quantity': 1,
						}
					}
					
				}

			})
			/*console.log(rooms)*/
			localStorage.setItem('rooms', JSON.stringify(session_rooms))
			session_rooms.quantity = 3

			console.log(JSON.parse(localStorage.getItem('rooms')))
			// var rooms_added = JSON.parse(localStorage.getItem('rooms'))
			// localStorage.setItem('rooms_added', JSON.stringify(rooms_added))
			// console.log(JSON.parse(localStorage.getItem('rooms_added')))
		})
		console.log(JSON.parse(localStorage.getItem('rooms')))
		// JSON.parse(localStorage.removeItem('rooms'))

		$('#btn-rooms_count').on('click', function(){
			var session_rooms = JSON.parse(localStorage.getItem('rooms'))
			var room_keys = []
			$.each(session_rooms, function(key, data){
				if(data.id > 0){
					room_keys.push(data.id)
				}
			})
			if(room_keys.length > 0){
				$.ajax({
					url : '/session-rooms',
					method: 'GET',
					dataType: 'json',
					data: {
						id : room_keys
					},
					success : function(e){
						$('#roomTable tbody tr').remove()
						$.each(e.result.rooms, function(key, data){
							$('#roomTable tbody')
								.append(
									'<tr>'
									 	+'<td><img src ="../../uploads/room_images/'+(data.image ? data.image : 'not-available.jpg')+'" height="60" width = "60"></td>'
									 	+'<td style="text-transform:uppercase">'+data.name+'</td>'
									 	+'<td><button style ="padding: 4px 10px;" class ="btn btn-default btn-flat btn-increment" id ="'+data.id+'"> + </button> <input type="text" style ="width: 15%; display:inline;" class ="session_rooms_quantity form-control" value="'+session_rooms[data.id].quantity+'"> <button style ="padding: 4px 10px;" class ="btn btn-default btn-flat btn-decrement" id ="'+data.id+'"> - </button></td>'
									 	+'<td>'+(data.rate * session_rooms[data.id].quantity *localStorage.getItem("night_duration")).toFixed(2)+'</td>'
									 	+'<td><button class ="btn btn-danger btn-flat btn-remove-row-room" id="'+data.id+'">X</button></td>'
									 +'</tr>'
								)
						})
					}

				})
			}
			
		})

		$('.btn-book').on('click',function(){
			var session_rooms = JSON.parse(localStorage.getItem('rooms'))
			var room_keys = []
			$.each(session_rooms, function(key, data){
				if(data.id > 0){
					room_keys.push(data.id)
				}
			})
		 	if(room_keys.length > 0){
		 		window.location = "/book-info"
		 	}
		 	else{
		 		alert('no rooms added')
		 	}
			
		})
		

	</script>
@stop

