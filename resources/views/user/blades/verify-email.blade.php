
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Verify</title>
	<link href="template/img/apple-touch-icon.png" rel="apple-touch-icon">
	<link href="https://fonts.googleapis.com/css?family=Open+	Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="template/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="template/lib/animate/animate.min.css" rel="stylesheet">
	<link href="template/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="template/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="template/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
	<link href="template/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="template/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  	<link href="custom/user.css" rel="stylesheet">
	<style>
		body{
			background-color: #f2f2f2;
		}
		.row div{
			/*background-color:#171a21;*/
			background-color:white;
			border-radius: 3px;
		}				
		#verify{
			width: 100% !important;
			margin-top: 15px;
			margin-bottom: 15px;
		    padding: 10px 10px;
		    border-radius: 0;
		    background: #50d8af;
		    color: white;
		    font-family: "Raleway", sans-serif;
		    letter-spacing: 1px;
		    font-weight: bold;
		    font-size: 13px;
		    text-transform: uppercase;
		}
		h3{
			color: #1de2c7;
		}
		p{
			color: #34394f;
		    font-family: "Raleway", sans-serif;
		    font-size: 12px;
		    text-align:justify;
		    letter-spacing: 1px;
		    margin: 10px 10px 30px 10px !important;
		}
		h4{
			margin-bottom: 30px !important;
		}
	</style>
</head>
<body>
	<div class ="container">
		<div class="row">
			<div class="col-md-4">
				<center><h3>Sea and River View</h3></center>
				<h4>Hello, {{ $data['name'] }}</h4>
				<p>In order to help maintain the security of your account, please verify your email address.</p>
				<a href="{{ $data['domain'] }}/verify-email/{{ $data['token'] }}" id="verify" class="btn default btn-block"> Verify </a>
			</div>
		</div>
	</div>
</body>
</html>