@extends('user.layouts.master-user')
@section('page-css')

@stop

@section('content')
	<div class="container">
		<div id="logo" class="pull-left"></div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li><a href="{{url('/')}}">Home</a></li>
					<li class="menu-active"><a href="{{url('/rooms')}}">rooms</a></li>
					<li><a href="#about">About Us</a></li>	
					<li><a href="#contact">Contact us</a></li>

				</ul>
			</nav>
		</div>
	<section id ="book_information" class ="infromation_book">
		<div class="container">
			<form id="frmBookInfo">
				<div class ="col-md-6">
					<input type ="text" class ="form-control" name ="first_name" id ="first_name"placeholder="first name">
					<input type ="text" class ="form-control" name ="last_name" id ="last_name" placeholder="last name">
					<input type ="text" class ="form-control"  name ="mobile_no" id="mobile_no" placeholder="phone number">
					<input type ="text" class ="form-control" name ="email" id ="email" placeholder="email">
					<a href ="javascript:;" class ="btn btn-success" id="btn-book-info"> submit </a>
				</div>
				<div class ="col-md-6">
					<div class="booking_details">
					</div>
				</div>
			</form>
		</div>
	</section>


	
	@include('user.blades.modals.modal')
@stop

@section('page-scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			if(localStorage.getItem('id')){
				$.ajax({
					url: '/get-user',
					method: 'get',
					dataType: 'json',
					data: {
						id : localStorage.getItem('id')
					},
					success : function(e){
						$.each(e.result, function(key, data){
							$('#first_name').val(data.first_name)
							$('#last_name').val(data.first_name)
							$('#mobile_no').val(data.mobile_no)
							$('#email').val(data.email)
						})
						
					}
				})
			}
			var session_rooms = JSON.parse(localStorage.getItem('rooms'))
			var room_keys = []
			$.each(session_rooms, function(key, data){
				if(data.id > 0){
					room_keys.push(data.id)
				}
			})
		 	if(room_keys.length < 1){
		 		window.location = "/rooms"
		 	}
		 	$.ajax({
		 		url : '/session-rooms',
		 		method : 'GET',
		 		dataType : 'JSON',
		 		data: {
		 			id : room_keys
		 		},
		 		success : function(e){
		 			var room = '';
		 			$.each(e.result.rooms, function(key, data){	
		 					room += ', '+data.name;
		 				});
		 			console.log(room.substring(2))
		 			var data = '<p> Checkin Date:'+ localStorage.getItem("checkin") +'</p>'
		 				+ '<p> Checkout Date:'+ localStorage.getItem("checkout") +'</p>'
		 				+ '<p class ="text"> Room Type: '+ room.substring(2)
		 				+'<p>';
		 			$('.booking_details').html(data);
		 		}
		 	})
			
		})
		$('#btn-book-info').on('click', function(){
			var input = $('#frmBookInfo').serializeArray()
	        var formData = new FormData()
	        $.each(input,function(key,input){
	            formData.append(input.name,input.value);
	        });
	        $.ajax({
	        	url: "session-book-info",
	            type: "post",
	            dataType: "json",
	            contentType: false,
	            processData: false,
	            cache: false,
	            data: formData,
	            success: function(e){
	            	var session_book_info = {
	            		'first_name': e.data.first_name,
	            		'last_name':e.data.last_name,
	            		'mobile_no':e.data.mobile_no,
	            		'email': e.data.email
	            	}
	            	localStorage.setItem('session_book_info', JSON.stringify(session_book_info))	
		        }
	   	 	})
		})

	</script>
@stop

