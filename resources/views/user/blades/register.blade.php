
@extends('user.layouts.master-user')
@section('page-css')
<style>
	.form-group{
		margin-bottom: 15px !important;
	}
	.error{
		border: 1px solid red;
	}
</style>
@stop

@section('content')
@include('alerts.alert-msg')
<section id="register-id" class ="register-class">
	<div class ="container">

		<div class="row">
			<!-- <div class ="col-md-7">

			</div> -->
			<div class ="col-md-4 col-md-offset-4">
				<div class ="col-md-12">
					<form id="frmRegister">
						<h3 class="control-label" id="signup-label">Sign Up</h3>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="firstname"><i class="fa fa-user"></i></label>
							  <input type="text" class="form-control" id="firstname" placeholder="First Name" aria-describedby="basic-addon3">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="lastname"><i class="fa fa-child"></i></label>
							  <input type="text" class="form-control" id="lastname" placeholder="Last Name" aria-describedby="basic-addon3">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="phonenumber"><i class="fa fa-phone"></i></label>
							  <input type="number" class="form-control" id="phonenumber" placeholder="Phone Number" aria-describedby="basic-addon3">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="uname"><i class="fa fa-user"></i></label>
							  <input type="text" class="form-control" id="uname" placeholder="Username" aria-describedby="basic-addon3">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="email"><i class="fa fa-envelope-square"></i></label>
							  <input type="email" class="form-control" id="email" placeholder="Email" aria-describedby="basic-addon3">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="password1"><i class="fa fa-lock"></i></label>
							  <input type="password" class="form-control" id="password1" placeholder="Password" aria-describedby="basic-addon3">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
							  <label class="input-group-addon" for="password2"><i class="fa fa-unlock-alt"></i></label>
							  <input type="password" class="form-control" id="password2" placeholder="Confirm Password" aria-describedby="basic-addon3">
							</div>
						</div>
						<button type="button" id="signup" class="btn blue" data-dismiss="modal">
		                     <i class="fa fa-check"></i> Submit 
	                	</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('page-scripts')
<script type="text/javascript">
	$( document ).ready(function() {
		
		console.log(Object.keys(localStorage));

		function isEmail(email) {
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  return regex.test(email);
		}

		$('#signup').click(function(){
			var fname = $('#firstname').val();
			var lname = $('#lastname').val();
			var pnumber = $('#phonenumber').val();
			var email = $('#email').val();
			var username = $('#uname').val();
			var password1 = $('#password1').val();
			var password2 = $('#password2').val();

			if (password1 == password2 && fname && lname && isEmail(email)) {
				$('#frmRegister').find("*").removeClass("error");
				$.ajax({
					url : '/register-save',
					method: 'GET',
					dataType: 'json',
					data: {
						fname:fname,
						lname:lname,
						pnumber:pnumber,
						email:email,
						username:username,
						password:password1,
						// password2:password2,
					},
					success : function(e){
						console.log(e);
						if (e.status == 0) {
							if (e.errors.username) {
								$('#uname').parent().addClass("error");
							}
							if (e.errors.email) {
								$('#email').parent().addClass("error");
							}
							if (e.errors.password) {
								$('#password1').parent().addClass("error");
								$('#password2').parent().addClass("error");
							}
						}else{
							// location.reload();
							$.ajax({
								url : '/signin',
								method: 'POST',
								dataType: 'json',
								data: {
									username:username,
									password:password1,
								},
								success : function(a){
									if(e.status == 1){
										localStorage.setItem('username' , a.data.username);
										localStorage.setItem('id' , a.data.id);
										location.reload();
									}
								},error: function(a){

								}
							});
						}
					},error: function(e){

					}

				})
			}else{
				$('#frmRegister').find("*").removeClass("error");

				if ($.isEmptyObject(fname)) {
					$('#firstname').parent().addClass("error");
				}
				if ($.isEmptyObject(lname)) {
					$('#lastname').parent().addClass("error");
				}
				if ($.isEmptyObject(email) || !isEmail(email)) {
					$('#email').parent().addClass("error");
				}
				if ($.isEmptyObject(username)) {
					$('#uname').parent().addClass("error");
				}
				if ($.isEmptyObject(password1)) {
					$('#password1').parent().addClass("error");
				}
				if ($.isEmptyObject(password2)) {
					$('#password2').parent().addClass("error");
				}

				if (password1 != password2 && !$.isEmptyObject(password1) && !$.isEmptyObject(password2)) {
					$('#password1').parent().addClass("error");
					$('#password2').parent().addClass("error");
				}
			}

		});
	});
</script>
@stop