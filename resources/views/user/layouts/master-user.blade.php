<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sea and River View</title>
	<meta content="" name="keywords">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta content="" name="description">
	<link href="template/img/apple-touch-icon.png" rel="apple-touch-icon">
	<link href="https://fonts.googleapis.com/css?family=Open+	Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="template/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="template/lib/animate/animate.min.css" rel="stylesheet">
	<link href="template/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="template/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="template/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
	<link href="template/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="template/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
	<link href="custom/user.css" rel="stylesheet">
	@yield('page-css')
</head>

<body id="body">
	<section id="topbar" class="d-lg-block">
		@include('user.included.top-bar')
	</section>

<!-- 	<header id="header">
		@include('user.included.header')
	</header> -->
	<div id ="content">
		@yield('content')
	</div>
	<footer id="footer">
		@include('user.included.footer')
	</footer>
	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<script type="text/javascript" src ="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="template/lib/easing/easing.min.js"></script>
	<script src="template/lib/superfish/hoverIntent.js"></script>
	<script src="template/lib/superfish/superfish.min.js"></script>
	<script src="template/lib/wow/wow.min.js"></script>
	<script src="template/lib/owlcarousel/owl.carousel.min.js"></script>
	<script src="template/lib/magnific-popup/magnific-popup.min.js"></script>
	<script src="template/lib/sticky/sticky.js"></script>
	<script src="template/contactform/contactform.js"></script>
	<script src="template/js/main.js"></script>
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../../bower_components/moment/min/moment.min.js">	</script>
	<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<script src="../../dist/js/adminlte.min.js"></script>
	<script src="../../dist/js/demo.js"></script>	
	<script type="text/javascript">
		$.ajaxSetup({
	  		headers: {
	    		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  		}
		});
	</script>
	@yield('page-scripts')
	<script type="text/javascript">
		
	$('#btn-login').click(function(){
		$.ajax({
			url : '/signin',
			method : "POST",
			dataType : "JSON",
			data : {
				username: $('#username').val(),
				password: $('#password').val(),
			},
			success : function (e){
				if(e.status == 1){
					
					// console.log(e.data.username)
					localStorage.setItem('username' , e.data.username)
					localStorage.setItem('id' , e.data.id)
					location.reload()
				}
			}

		})
	})

    $('#frmLogin input, #register-id input').focus(function(){
    	$(this).css("border-bottom","3px solid #50d8af");
    });
    $('#frmLogin input, #register-id input').focusout(function(){
    	$(this).css("border-bottom","1px solid #50d8af");
    });

	if(localStorage.getItem('id')){
		$('.user')
		.html(''
		+'<ul class="nav-menu">'
          +'<li class="menu-has-children"><a href="javascript:;">'+localStorage.getItem('username')+'</a>'
            +'<ul style ="z-index:100000;">'
              +'<li><a href="javascript:;">My Profile</a></li>'
              +'<li><a href="javascript:;" class ="signout">Signout</a></li>'
            +'</ul>'
          +'</li>'
          +'</ul>'
        +'')
	}
	else{
		$('.user')
		.html(''
		+'<ul class="nav-menu">'
          +'<li class="menu-has-children"><a href="javascript:;" class ="login-user" data-toggle="modal" data-target="#user-modal">Sign in Now</a>'
          +'</li>'
          +'</ul>'
        +'')

	}
	$('.signout').click(function(){
		localStorage.removeItem('id')
		localStorage.removeItem('username')
		location.reload();
		$.ajax({//remove php session
			url : '/logout',
			method: 'get',
			dataType: 'json',
			data: {
			},
			success : function(a){
			},error: function(a){

			}
		});
	})
	$('#frmLogin').keydown(function(event){
    	if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
	</script>

</body>
</html>
