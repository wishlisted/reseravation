@extends('admin.admin-layout.master-admin')

@section('page-css')
<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<section class ="content">
    @include('alerts.alert-msg')
    <div class="row" id ="follow">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject text-uppercase"> Users Information</span>
                    </div>
                    <div class="actions">
                        <a href="{{url('/dashboard')}}" class="btn btn-default btn-flat">
                            <i class="fa fa-arrow-left"></i> Back
                        </a>
                    </div>
                </div>
                <div class="portlet-body" style="padding: 0 15px">
                  
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-scripts')
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
@stop
