@if(Session::get('success-msg'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <center> <strong>Well done!</strong> {!! Session::get('success-msg') !!}</center>
    </div>

@elseif(Session::get('error-msg'))
	<div class="alert alert-error alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <center> <strong>Error!</strong> {!! Session::get('error-msg') !!}</center>
    </div>

@elseif(Session::get('warning-msg'))
	<div class="alert alert-warning" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <center> <strong>Warning!</strong> {!! Session::get('warning-msg') !!}</center>
    </div>

@elseif(Session::get('info-msg'))
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <center> <strong>Oooops!</strong> {!! Session::get('info-msg') !!}</center>
    </div>
@endif