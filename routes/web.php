<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*----------------DEFAULT------------------------*/
Auth::routes();
Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
/*----------------ADMIN----------------------*/
Route::get('dashboard', 'admins\DashboardController@index');
Route::get('room/index', 'admins\RoomController@index');
Route::get('room/form/{id}', 'admins\RoomController@form');
Route::post('room/save/{id}', 'admins\RoomController@save');
Route::post('room/delete', 'admins\RoomController@delete');
Route::get('admin','admins\UsersController@index');
Route::get('admin/users/{id}','admins\UsersController@getUsers');
Route::get('admin/register','admins\UsersController@getRegister');
Route::post('admin/save','admins\UsersController@postSave');
Route::post('admin/delete','admins\UsersController@postDelete');
Route::get('walkin-reservation/index','admins\WalkinReservationController@index');	
Route::get('walkin-reservation/form/{id}','admins\WalkinReservationController@form');	
Route::post('walkin-reservation/save/{id}', 'admins\WalkinReservationController@save');


Route::get('walkin-reservation/room-type', 'admins\WalkinReservationController@room_type');
Route::get('walkin-reservation/room-number','admins\WalkinReservationController@checkRoomAvailable');
Route::get('walkin-reservation/rate','admins\WalkinReservationController@get_rate');

Route::get('dashboard/sales', 'admins\DashboardController@sales');
Route::get('system-settings/', 'admins\DashboardController@SystemSettings');
Route::get('system-settings/save/{id}', 'admins\DashboardController@SystemSettingsSave');


/*----------------USER----------------------*/
Route::get('/', 'users\HomepageController@index');
Route::post('check-availability', 'users\HomepageController@check_availabilty');
Route::get('rooms','users\HomepageController@rooms');
Route::get('session-rooms','users\HomepageController@getRoomsById');
Route::get('room-type', 'users\HomepageController@getRooms');
Route::get('room-capacity', 'users\HomepageController@getCapacity');
Route::post('add-room', 'users\HomepageController@postAddRoom');
Route::get('signup', 'users\HomepageController@getRegister');
Route::post('signin', 'users\HomepageController@postLoginUser');
Route::get('book-info','users\ReserveRoomController@getIndex');
Route::get('get-user','users\ReserveRoomController@getUser');
Route::post('session-book-info', 'users\ReserveRoomController@postSessionBookInfo');
Route::get('register-save','users\HomepageController@RegisterSave');
Route::get('logout','users\HomepageController@logout');

Route::get('send-email','HomeController@email');
Route::get('verify-email/{token}','users\HomepageController@verify');


Route::any('home',function(){
    return Redirect::to('dashboard');
});