var ChartsFlotcharts = function() {

    return {
        //main function to initiate the module

        init: function() {

            Metronic.addResizeHandler(function() {
                Charts.initPieCharts();
            });

        },

        initCharts: function() {

            if (!jQuery.plot) {
                return;
            }

            var data = [];
            var totalPoints = 250;

            //Interactive Chart
            var type = 1;

            var currentdate = new Date();
            // var datetime1 = currentdate.getFullYear() + "/" + currentdate.getMonth() + "/" + currentdate.getDate();
            var date_year = currentdate.getFullYear();
            var date_month = currentdate.getMonth()+1;

            $('select[name="year"]').val(date_year);
            $('select[name="month"]').val(date_month);

            function chart2() {

                if ($('#chart_2').length != 1) {
                    return;
                }

                // var obj = document.getElementsByClassName("active");
                // var type = 0;

                // if (obj[1].id == "btn-daily") {
                //     type = 1;
                // }else if(obj[1].id == "btn-monthly"){
                //     type = 2;
                // }

                var year = $('select[name="year"] :selected').val();
                var month = $('select[name="month"] :selected').val();

                var visitors = [
                    [1, "1,111,111.00"],
                    [2, "2,222,222,.00"],
                    [3, "3,333,333.00"],
                ];
                        console.log(visitors)
                        $('.alert-sales').remove();

                            var plot = $.plot($("#chart_2"), [{
                                // data: data.data,
                                data: visitors,
                                label: "Sales",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            }], {
                                series: {
                                    lines: {
                                        show: true,
                                        lineWidth: 2,
                                        fill: true,
                                        fillColor: {
                                            colors: [{
                                                opacity: 0.25
                                            }, {
                                                opacity: 0.01
                                            }]
                                        }
                                    },
                                    points: {
                                        show: true,
                                        radius: 5,
                                        lineWidth: 5
                                    },
                                    shadowSize: 2
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: "#eee",
                                    borderColor: "#eee",
                                    borderWidth: 1
                                },
                                colors: ["#0d1217", "#37b7f3", "red"],
                                xaxis: {
                                    ticks: 10,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                },
                                yaxis: {
                                    ticks: 4,
                                    tickColor: "#eee",
                                    tickDecimals: 0,
                                    tickFormatter: function numberWithCommas(x) {
                                          return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
                                    }
                                },
                            });
                            // function makeDummyDiv(labels, width) {
                            // return $('' + ''+ labels.join("") + '').appendTo(placeholder);
                            // }

                            function showTooltip(x, y, contents) {
                                $('<div id="tooltip">' + contents + '</div>').css({
                                    position: 'absolute',
                                    display: 'none',
                                    top: y + 5,
                                    left: x + 15,
                                    // border: '1px solid #333',
                                    padding: '4px',
                                    color: '#fff',
                                    'border-radius': '100px',
                                    'background-color': '#0c3315',
                                    // opacity: 0.80
                                }).appendTo("body").fadeIn(200);
                            }

                            var previousPoint = null;
                            $("#chart_2").bind("plothover", function(event, pos, item) {
                                $("#x").text(pos.x.toFixed(2));
                                $("#y").text(pos.y.toFixed(2));

                                if (item) {
                                    if (previousPoint != item.dataIndex) {
                                        previousPoint = item.dataIndex;

                                        $("#tooltip").remove();
                                        var x = item.datapoint[0].toFixed(2),
                                            y = Number(item.datapoint[1].toFixed(2));

                                        var z = "";

                                        if (type == 2) {
                                            switch(x){
                                                case "1.00":
                                                    z = "January";
                                                break;
                                                case "2.00":
                                                    z = "February";
                                                break;
                                                case "3.00":
                                                    z = "March";
                                                break;
                                                case "4.00":
                                                    z = "April";
                                                break;
                                                case "5.00":
                                                    z = "May";
                                                break;
                                                case "6.00":
                                                    z = "June";
                                                break;
                                                case "7.00":
                                                    z = "July";
                                                break;
                                                case "8.00":
                                                    z = "August";
                                                break;
                                                case "9.00":
                                                    z = "September";
                                                break;
                                                case "10.00":
                                                    z = "October";
                                                break;
                                                case "11.00":
                                                    z = "November";
                                                break;
                                                case "12.00":
                                                    z = "December";
                                                break;
                                            }
                                        }else{
                                            switch(month){
                                                case "1":
                                                    z = "Jan "+Number(x);
                                                break;
                                                case "2":
                                                    z = "Feb "+Number(x);
                                                break;
                                                case "3":
                                                    z = "Mar "+Number(x);
                                                break;
                                                case "4":
                                                    z = "Apr "+Number(x);
                                                break;
                                                case "5":
                                                    z = "May "+Number(x);
                                                break;
                                                case "6":
                                                    z = "Jun "+Number(x);
                                                break;
                                                case "7":
                                                    z = "Jul "+Number(x);
                                                break;
                                                case "8":
                                                    z = "Aug "+Number(x);
                                                break;
                                                case "9":
                                                    z = "Sept "+Number(x);
                                                break;
                                                case "10":
                                                    z = "Oct "+Number(x);
                                                break;
                                                case "11":
                                                    z = "Nov "+Number(x);
                                                break;
                                                case "12":
                                                    z = "Dec "+Number(x);
                                                break;
                                            }
                                        }

                                        function formatMoney(n) {
                                            return (Math.round(n * 100) / 100).toLocaleString();
                                        }

                                        showTooltip(item.pageX, item.pageY, "<strong>&#8369; "+formatMoney(y)+"</strong></br><small>"+z+"</small>");
                                    }
                                } else {
                                    $("#tooltip").remove();
                                    previousPoint = null;
                                     // $('.sales').html("&#8369; "+sales+"       <label>avg</label>");
                                    // $('.sales').text("asd");
                                }
                            });
                
                // console.log(type);

            }

            chart2();

            // setInterval(function(){
            //     chart2();
            // }, 0);

            // console.log(date_year, date_month);

            $('select[name="year"]').on('change', function()
            {
                // type = 2;
                chart2();
            });
            $('select[name="month"]').on('change', function()
            {
                type = 1;
                chart2();
            });
            $('#btn-daily').click(function(){
                $('#month').show();
                type = 1;
                chart2();
            });
            $('#btn-monthly').click(function(){
                $('#month').hide();
                type = 2;
                chart2();
            });
        }

    };

}();