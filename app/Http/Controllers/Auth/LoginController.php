<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use App\User;
use Hash;
use Crypt;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $username = 'username';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {

            return back()->withInput()->withErrors($validator);
        } else {
            $user = User::where('username',trim($request->input('username')))->first();
            if(!is_null($user)){
                $salt = $user->salt;
                $user = User::where('username', $request->username)
                            ->where('status', 1)
                            ->first();
                if(Hash::check($request->password, $user->password) == false){
                    $request->session()->flash('error-msg', 'Ooops! Invalid log in credentials.');
                return back();
                }
            }

            if(!is_null($user)){
                Auth::login($user);//pr($user);
                $request->session()->flash('success-msg', 'You have successfully log in.');
                return redirect()->intended('/dashboard');
            }else{
                $request->session()->flash('error-msg', 'Ooops! Invalid log in credentials.');
                return back();
            }
        }
    }
}
