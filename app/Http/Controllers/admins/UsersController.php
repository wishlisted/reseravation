<?php

namespace App\Http\Controllers\admins;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\DynamicFunction;
use DB;
use Session;
use File;
use Validator;
use Auth;
use Hash;



class UsersController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){

    	
    	return view('admin.admin-views.personnels.index');
    }

    public function getUsers($id, Request $request){
    	$data['users'] = db::table('users')->where('type', $id)->where('status', 1)->get();
    	$data['type'] = $id == 1 ? 'Customers' : 'Admin';
    	return view('admin.admin-views.personnels.users', $data);
    }

    public function getRegister(Request $request){


    	return view('admin.admin-views.personnels.register');
    }

    public function postSave(Request $request){
    	$validator = Validator::make($request->all(), [
    		// 'name' => (($id == intval(0)) ? 'required' : ($id > 0 && $check == $request->name && $check_id != $id) ? 'required|unique:rooms' :'required'),
            'username' => 'required|unique:users|min:5|max:10',
    		'password' => 'required|min:6',
    		'confirm_password' => 'required|same:password',
    		'first_name' => 'required',
    	]);
        if($validator->fails()){
            return response()->json(array('status' => 0, 'errors' => $validator->errors()));
        }	
        else{
        	$user = [
        		'username' => $request->username,
        		'password' => hash::make($request->password),
        		'first_name' => $request->first_name,
        		'last_name' => !empty($request->last_name) ? $request->last_name: '',
        		'email' => !empty($request->email) ? $request->email: '',
        		'token' => str_random(50),
        		'type' => 0,
        		'status' => 1,
        		'created_at' => date("Y-m-d H:i:s"),
        	];

        	$register = DB::table('users')->insert($user);
        	if($register){
        		Session::flash('success-msg', 'Admin is successfuly registered!');
            return response()->json(array('status' => 1));
        	}
        	else{
        		Session::flash('danger-msg', 'Something Went Wrong!');
            	return response()->json(array('status' => 2));
        	}
        }	
    }
    public function postDelete(Request $request){
        foreach ($request->id as $key => $value) {
            DynamicFunction::delete_where('users', 'id', $value);
        }
        Session::flash('error-msg', 'User successfully deleted.');
        return response()->json(array('status' => 1));
    }
}
