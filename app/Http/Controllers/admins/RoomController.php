<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rooms;
use App\Models\Room_count;
use App\Models\DynamicFunction;
use Session;
use File;
use DB;
use Validator;

class RoomController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $data['rooms'] = Rooms::where('status', 1)->get();
    	return view('admin.admin-views.rooms.index', $data);
    }
    public function form($id, Request $request){
    	$data['info'] = Rooms::find($id);
        $data['room_count'] = Rooms::active_rooms($id);
    	
    	return view('admin.admin-views.rooms.form', $data);
    }
    public function save($id, Request $request){
    	$info = Rooms::find($id);

        $check = Rooms::where('name',$request->name)->where('status', 1)->value('name');
        $check_id = Rooms::where('name',$request->name)->where('status', 1)->value('id');
    	$validator = Validator::make($request->all(), [
    		// 'name' => (($id == intval(0)) ? 'required' : ($id > 0 && $check == $request->name && $check_id != $id) ? 'required|unique:rooms' :'required'),
            'name' => 'required',
    		'size' => 'required|numeric',
    		'capacity' => 'required|numeric',
    		'rate' => 'required|numeric',
    		'bed' => 'required|numeric',
            'description' => 'required',
    	]);
        if($validator->fails()){
            return response()->json(array('status' => 0, 'errors' => $validator->errors()));
        } else {
            if(empty($info)){
                $info = new Rooms;
                $info->created_at = DynamicFunction::timestamp();
            }
            $info->name = $request->name;
            $info->available = !empty($request->available) ? $request->available : '';
            $info->size = $request->size;
            $info->capacity = $request->capacity;
            $info->rate = $request->rate;
            $info->bed = $request->bed;
            $info->description = $request->description;

            if($request->hasFile('image')){
                $image = $request->file('image');
                $tempFileName = $image->getClientOriginalName();
                $image_type = substr($tempFileName, -4);
                //Check if filename is already existing
                $access = 1;
                while ($access == 1) {
                    $photo = str_random(10).$image_type;
                    $result = DB::table("room_type")->where("image", $photo)->count();
                    if($result > 0)
                        $access = 1;
                    else
                        $access = 0;
                }

                $path = "uploads/room_images";
                if(!File::exists($path)) {
                    $path2 = "uploads";
                    if(!File::exists($path2)) {
                        File::makeDirectory($path2, 0755);
                    }
                    File::makeDirectory($path, 0755);
                }
                $image->move($path, $photo);

                $info->image = $photo;
            } else {
                 $info->image = !empty($info->image)? $info->image : '';
            }
            $info->save();
            $delete_room = Room_count::where('room_type_id', $info->id)->whereNotIn('id', $request->room_id != null ? $request->room_id : [0])->delete();
            foreach($request->room_no as $key => $value) {
                if(!empty($request->room_id[$key])){
                    $update_room = Room_count::where('room_type_id', $info->id)->where('id', $request->room_id[$key])->update(['room_no' => $value]);
                }
                else{
                    if(!empty($value)){
                        $data = [
                            'room_no' =>  $value,
                            'room_type_id' =>  $info->id,
                            'created_at' => date("Y-m-d H:i:s")
                        ];
                        Room_count::insert($data);
                    }  
                }
            }




            // DynamicFunction::delete_permanent('room','room_id',$info->id);
            // if(!empty($request->room_no)){
            //     foreach($request->room_no as $key => $value){
            //         if(!empty($value)){
            //             $data = array(
            //             'room_no' => $value,
            //             'room_id' => $info->id,
            //             'created_at' => date("Y-m-d H:i:s")
            //             );
            //             DynamicFunction::insert_data('room',$data);
            //         }
                    
            //     }
            // }
                



            Session::flash('success-msg', 'Room is successfuly save!.');
            return response()->json(array('status' => 1, 'url' => url('room/form/'.$info->id)));
        }

    }
    public function delete(Request $request){
        foreach ($request->id as $key => $value) {
            DynamicFunction::delete_where('room_type', 'id', $value);
        }
        Session::flash('error-msg', 'Room successfully deleted.');
        return response()->json(array('status' => 1));
    }

}
