<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Helpers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Reserve;
use App\Models\DynamicFunction;
use DB;
use Session;
use File;
use Validator;
use Auth;
use DateTime;



class WalkinReservationController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $data['reserved'] = DB::table('reservation_details')->where('status','<>',0)->get();
    	
    	return view('admin.admin-views.walkin.index', $data);
    }

    public function form($id, Request $request){
        $check_id = DB::Table('reservation_details')->where('id', $id)->first();
        $data['food_fee'] = DB::table('system_settings')->value('allow_free_breakfast');
        if(empty($check_id)){
            $data['reference'] = Reserve::generateReferenceNumber(); 
        }
        else{
            $data['reference'] = dB::table('reservation_details as rd')
                        ->where('rd.id',$id)
                        ->value('rd.reference_number');
                    
                $data['rooms'] = DB::Table('reserved_room as rr')
                                ->join('room_type as rt','rt.id','=','rr.room_type_id')
                                ->join('room as r','r.id','=','rr.room_id')
                                ->select('r.room_no','rt.name','rr.*')
                                ->where('rr.reservation_details_id', $id)->get();

            if($check_id->customer_id == 0){
                $data['details'] = dB::table('reservation_details as rd')
                        ->leftjoin('walkin_customer as wc','wc.id','=','rd.walkin_customer_id')
                        ->where('rd.id',$id)
                        ->select('rd.id','rd.reference_number', 'wc.first_name','wc.last_name','wc.address','wc.contact_number','rd.guest_number',db::raw('date_format(rd.date_from, "%Y-%m-%d") as date_from'), db::raw('date_format(rd.date_to, "%Y-%m-%d") as date_to'), 'rd.payment_method_id', 'rd.total','rd.function_room','rd.videorent','rd.bonfire','rd.bananaboat','rd.corckage','rd.bar','rd.total_breakfast')
                        ->first();
                    }

        }

    	return view('admin.admin-views.walkin.form', $data);
    }
    public function room_type(){
        $room_type =  DB::table('room_type')->where('status', 1)->get();
        return response()->json(['data' => $room_type]);
    }
    public function get_rate(Request $request){
        $rate =  DB::table('room_type')->where('id', $request->id)->value('rate');
        return response()->json(['rate' => $rate]);
    }
    public function checkRoomAvailable(Request $request){
        $query = Reserve::check_room_availability($request->date_from.' '.DynamicFunction::InOut()[0]->checkin, $request->date_to.' '.DynamicFunction::InOut()[0]->checkout, $request->id);

        // $query = Reserve::check_room_availability('2018-06-10 01:46:12', '2018-06-10 01:46:12', $request->id);

        return response()->json(['data' => $query, 'status' => 1]);
    }


    public function save($id, Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'contact_number' => 'required|numeric',
            'guest_number' => 'required|numeric',
            'date_from' => 'required',
            'date_to' => 'required',
            'total' => 'required',
        ]);
        $check_exist = DB::table('reservation_details')->where('id' ,$id)->first();
        if($validator->fails()){
            return response()->json(array('status' => 0, 'errors' => $validator->errors()));
        }
        else{
            $return_id = DB::transaction(function() use($request, $id, $check_exist){
                $walkin_details = [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'contact_number' => $request->contact_number,
                    'address' => $request->address,
                ];
                $reservation_details = [
                    'guest_number' => $request->guest_number,
                    'payment_method_id' => $request->payment_method,
                    'customer_id' => 0,
                    'total' => $request->total,
                    'total_breakfast' => ($request->breakfasttotal ? $request->breakfasttotal : 0 ),
                    'function_room' => ($request->function_room ? $request->function_room : 0),
                    'videorent' => ($request->videorent ? $request->videorent : 0),
                    'bonfire' => ($request->bon_fire ? $request->bon_fire : 0),
                    'bananaboat' => ($request->bananaboat ? $request->bananaboat : 0),
                    'corckage' => ($request->corckage ? $request->corckage : 0),
                    'bar' => ($request->bar ? $request->bar : 0),
                    'transaction_id' => 1,
                    'notification_status' => 0,
                    'status' => 1,
                ];  

                $date = [
                    'date_from' => $request->date_from.' '.DynamicFunction::InOut()[0]->checkin,//
                    'date_to' => $request->date_to.' '.DynamicFunction::InOut()[0]->checkout,//
                ];

                $date_from = new DateTime($date['date_from']);
                $date_to = new DateTime($date['date_to']);
                $days_diffence = $date_to->diff($date_from)->d > 0 ? $date_to->diff($date_from)->d : 1; 
                if(empty($check_exist)){
                    $walkin_details['created_at'] = DynamicFunction::timestamp();
                    $walkin_id = DB::table('walkin_customer')->insertGetId($walkin_details);
                    $reservation_details['date_from'] = $request->date_from.' '.DynamicFunction::InOut()[0]->checkin;//
                    $reservation_details['date_to'] = $request->date_to.' '.DynamicFunction::InOut()[0]->checkout;//
                    $reservation_details['date'] = DynamicFunction::timestamp();
                    $reservation_details['reference_number'] = $request->reference_number;
                    $reservation_details['user_id'] = Auth::user()->id;
                    $reservation_details['walkin_customer_id'] = $walkin_id;
                    $reservation_details['created_at'] = DynamicFunction::timestamp();
                    $reservation_id = DB::table('reservation_details')->insertGetId($reservation_details);
                    foreach ($request->room_type as $key => $value) {
                        $data = [
                            'reservation_details_id' => $reservation_id,
                            'room_type_id' => $value,
                            'room_id' => $request->room_id[$key],
                            'rate' => intval($request->room_rate[$key]),
                            // 'food_fee' => Reserve::food_fee(intval($request->breakfast[$key])),
                            'food_fee' => intval($request->breakfast[$key]),
                            'total_rate' => intval($request->room_rate[$key]) * intval($days_diffence),
                            'discount_id' => 1,
                            'offer_id' => 1,
                            'created_at' =>DynamicFunction::timestamp(),
                        ];
                        DB::table('reserved_room')->insert($data);
                    }
                }
                else{

                }
                return $reservation_id;
            });

            Session::flash('success-msg', 'Reservation is sucessfully save!.');
            return response()->json(['status' => 1, 'url' => url('walkin-reservation/form/'.$return_id)]);
            
        }
    }

}