<?php

namespace App\Http\Controllers\admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Rooms;
use App\Models\DynamicFunction;
use Session;
use DB;

class DashboardController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    	$data['year'] = DB::select("select year(created_at) as y
                                    from reservation_details
                                    group by year(created_at);");

        $data['rooms'] = DB::select("select * 
                                    from room as r
                                    left join room_type as rt ON r.room_type_id = rt.id
                                    where r.status = 1 and r.id not in (select rr.room_id 
                                    from reserved_room as rr
                                    left join reservation_details as rd ON rd.id = rr.reservation_details_id
                                    where rd.status = 1)");
        $data['rooms2'] = DB::select("select * 
                                    from room as r
                                    left join room_type as rt ON r.room_type_id = rt.id
                                    where r.status = 1 and r.id in (select rr.room_id 
                                    from reserved_room as rr
                                    left join reservation_details as rd ON rd.id = rr.reservation_details_id
                                    where rd.status = 1)");

    	return view('admin.admin-views.dashboard.dashboard',$data);
    }
     public function Sales(Request $request){

        $data = Rooms::sales($request->all());

        // dd($data);
        return $data;
    }
    public function SystemSettings(Request $request){
        $data['system'] = DB::TABLE('system_settings')->first();

        return view('admin.admin-views.system-settings.index',$data);
    }
    public function SystemSettingsSave($id,Request $request){
        $id = intval($id);
        $data = array(
                // 'barcode_prefix' => $request->input('barcode_prefix'),
                'allow_free_breakfast' => (empty($request->input('price'))? 0 : $request->input('price')),
                'checkin' => (empty($request->input('checkin'))? 0 : $request->input('checkin')),
                'checkout' => (empty($request->input('checkout'))? 0 : $request->input('checkout')),
            );
        if(is_numeric($id) && $id != 1) {
            $update = DynamicFunction::update_by_id($table='system_settings',$data,$id);
        }else{
            $insert = DynamicFunction::insert_get_id($table='system_settings',$data);
        }
        Session::flash('success-msg', 'System Settings successfully saved.');

        return response()->json(array('status' => 1));
    }
}
