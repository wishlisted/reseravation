<?php

namespace App\Http\Controllers\users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rooms;
use App\Models\DynamicFunction;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Session;
use File;
use DB;
use Validator;
use App\User;
use Hash;
class HomepageController extends Controller
{

    public function __construct()
    {
        
    }
    public function index(){
    	
    	return view('user.blades.home');
    }

    public function check_availabilty(Request $request){
    	$validator = Validator::make($request->all(), [
    			'checkin'=> 'required',
    			'checkout'=> 'required',
    			'rooms' => 'required'
    	]);
    	if($validator->fails()){
    		return response()->json(array('status' => 0, 'errors' => $validator->errors()));
    	}
    	else{
    		return response()->json(['status' => 1]);
    	}
    }

    public function rooms(Request $request){
        $data['rooms'] = Rooms::where('status', 1)->get();


    	return view('user.blades.rooms', $data);
    }


    public function getRooms(Request $request){
        $data['rooms'] = Rooms::where('status', 1)->get();

        return response()->json(array('status' => 1, 'result'=> $data));
    }

    public function getRoomsById(Request $request){
        $data['rooms'] = Rooms::whereIn('id',$request->id)->where('status', 1)->get();

        return response()->json(array('status' => 1, 'result'=> $data));
    }

    public function getCapacity(Request $request){
        $room_id = $request->id;
        $data['capacity'] = Rooms::where('id', $room_id)->where('status', 1)->get();

        return response()->json(['status' => 1, 'result'=>$data]);
    }

    public function postAddRoom(Request $request){
        

        return response()->json(['status' => '1']);
    }

    public function getRegister(Request $request){
        if ($request->session()->get('user')) {
            return redirect('/');
        }else{
            return view('user.blades.register');
        }
    }

    public function RegisterSave(Request $request){
        $rules = [
            "fname" => "required",
            "lname" => "required",
            "email" => "required",
            "username" => "required|unique:users|min:5|max:10'",
            "password" => "required|min:6"
        ];
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()) {
            return response()->json(array('status' => 0,'errors' => $validator->errors()));
        } else {
            $data = [
                "first_name" => $request->fname,
                "last_name" => $request->lname,
                "email" => $request->email,
                "mobile_no" => $request->pnumber,
                "username" => $request->username,
                'token' => str_random(50),
                "password" => hash::make($request->password),
                "type" => "1",
                "status" => "1",
                'created_at' => date("Y-m-d H:i:s"),
            ];

            $register = DB::table('users')->insert($data);
            Mail::send(new SendMailable(['domain'=>$_SERVER['SERVER_NAME'],'name' => $data['first_name'],'email' => $data['email'], 'token' => $data['token']]));

            if($register){
                Session::flash('success-msg', 'Successfuly registered!');
            return response()->json(array('status' => 1));
            }
            else{
                Session::flash('danger-msg', 'Something Went Wrong!');
                return response()->json(array('status' => 2));
            }
        }
    }

    public function postLoginUser(Request $request){
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {

            return response()->json(['status' => 2, 'message' => 'kulang yung info']);
        } else {
            $user = User::where('username',trim($request->input('username')))->where('type', 1)->first();
            if(!is_null($user)){
                $salt = $user->salt;
                $user = User::where('username', $request->username)
                            ->where('type', 1)
                            ->first();
                if(Hash::check($request->password, $user->password) == false){
                  return response()->json(['status' => 3, 'message' => 'Wrong Credentials']);
                }
                else{
                    $result = [
                        'id' => $user->id,
                        'username' => $user->username,
                    ];
                    $request->session()->put('user', $result);
                    // dd(Session::get('user'));
                    return response()->json(['status' => '1', 'data' => $result]);
                }
            }
            else{
                return response()->json(['status' => 3, 'message' => 'Wrong Credentials']);
            }
        }

    }
    public function logout(Request $request){
        $request->session()->forget('user');
    }
    public function Verify($token,Request $request){
        $check = DB::table('users')->where('token',$token)->value('verified');

        if (isset($check) && $check == 0) {
            db::table('users')->where('token',$token)->update(['verified'=> 1]);
        }

        return redirect('/');

    }
}
