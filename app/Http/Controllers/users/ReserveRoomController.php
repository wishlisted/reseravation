<?php

namespace App\Http\Controllers\users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rooms;
use App\Models\DynamicFunction;
use Session;
use File;
use DB;
use Validator;
use App\User;
use Hash;

class ReserveRoomController extends Controller
{
	public function getIndex(Request $request){


		return view('user.blades.book-info');
	} 
    public function postCheckAvailability(Request $request){
    	
    }
    public function getUser(Request $request){
    	$user = User::where('id', $request->id)->get();


    	return response()->json(['status' => 1, 'result' => $user]);
    }
    public function postSessionBookInfo(Request $request){
    	$rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_no' => 'required',
            'email' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()) {

            return response()->json(['status' => 2, 'message' => 'kulang yung info']);
        } 
        else{
        	return response()->json(['status' => 1, 'data' => $request->all()]);
        }
    	
    }

}
