<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    protected $table = 'room_type';

    public static function active_rooms($id){

    	return $query = DB::table('room')->where('room_type_id', $id)->get();
    }
    public static function sales($request){
        if($request['year'] == null){
            $data['data'] = [0,0,0,0,0,0,0,0,0,0];
            $data['label'] = [1,2,3,4,5,6,7,8,9,10];
            return $data;
        }
    	if ($request['type'] == 1) {
    		$query = DB::SELECT('select * , year(created_at) as year, month(created_at) as month, day(created_at) as day
					from reservation_details
					where year(created_at) = "'.$request['year'].'" and month(created_at) = "'.$request['month'].'"');

    		$num_of_days = cal_days_in_month(CAL_GREGORIAN,$request['month'],$request['year']);
    		$data['label'] = [];
    		$data['data'] = [];
    		for ($i=0; $i<=$num_of_days ; $i++) { 
    			$data['data'][$i] = 0;
    			$data['label'][$i] = $i;
    		}
    		foreach ($data['data'] as $key => $value) {
    			if ($query) {
    				foreach ($query as $key1 => $value1) {
	    				if (($value1->day - 1) == $key) {
	    					$data['data'][$key] += $value1->total;
	    				}
	    			}
    			}
    		}
    		// for ($i=1; $i<=$num_of_days ; $i++) { 
    		// 	array_push($data['label'],$i);
    		// 	if ($query) {
    		// 		foreach ($query as $key => $value) {
	    	// 			if ($value->day == $i) {
	    	// 				array_push($data['data'],$value->total);
	    	// 			}else{
	    	// 				array_push($data['data'],0);
	    	// 			}
	    	// 		}
    		// 	}else{
    		// 		array_push($data['data'],0);
    		// 	}
    		// }
    		// dd($query,$data);
    		return $data;

    	}elseif ($request['type'] == 2) {
    		$query = DB::SELECT('select * , year(created_at) as year, month(created_at) as month, day(created_at) as day
					from reservation_details
					where year(created_at) = "'.$request['year'].'"');

    		$num_of_days = cal_days_in_month(CAL_GREGORIAN,$request['month'],$request['year']);
    		$data['label'] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    		$data['data'] = [];
    		for ($i=0; $i<=11 ; $i++) { 
    			$data['data'][$i] = 0;
    		}
    		foreach ($data['data'] as $key => $value) {
    			if ($query) {
    				foreach ($query as $key1 => $value1) {
	    				if (($value1->month - 1) == $key) {
	    					$data['data'][$key] += $value1->total;
	    				}
	    			}
    			}
    		}
    		// dd($query,$data);
    		return $data;
    	}
    }
}
