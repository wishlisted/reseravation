<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use DateTime;
use DateTimeZone;
class DynamicFunction extends Model
{
	public static function get_all($table)
	{
		return DB::table($table)->where('status','<>',0)->get();
	}

	public static function get_where($table,$column,$value)
	{
		return DB::table($table)->where($column,$value)->get();
	}

	public static function get_where_first($table,$column,$value)
	{
		return DB::table($table)->where($column,$value)->first();
	}

	public static function insert_get_id($table,$data)
	{
		return DB::table($table)->insertGetId($data);
	}

	public static function get_by_id($table,$id)
	{
		$query = DB::table($table)->where('id',$id)->first();
		if($query) {
			return $query;
		}
		return false;
	}

	public static function update_by_id($table,$data,$id)
	{	

		$query = DB::table($table)->where('id',$id)->update($data);
		if($query) {
			return $query;
		}
		return false;
	}

	public static function delete_where($table,$column,$value)
	{
		return DB::table($table)->where($column,$value)->update(array('status' => '0'));
	}

	public static function insert_data($table,$data)
	{
		return DB::table($table)->insert($data);
	}

	public static function delete_permanent($table,$column,$value)
	{
		return DB::table($table)->where($column,$value)->delete();
	}

	public static function update_status($table,$id,$status)
	{
		return DB::table($table)->where('id',$id)->update(array('status' => $status));
	}

	public static function update_where($table,$column,$value,$data)
	{
		return DB::table($table)->where($column,$value)->update($data);
	}

	public static function current_time(){
		 $datetime = new DateTime(null, new DateTimeZone('Asia/Manila'));
	return $datetime->format('H:i:s');
	}
	public static function timestamp(){
		 $datetime = new DateTime(null, new DateTimeZone('Asia/Manila'));
	return $datetime->format('Y-m-d H:i:s');
	}
	public static function InOut(){
		return DB::select('select checkin, checkout from system_settings');
	}




}
