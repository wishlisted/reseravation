<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Reserve extends Model
{
    public static function generateReferenceNumber(){
    	$count_transaction = DB::table('reservation_details')->count();
    	$prefix = 'SRV';
    	$year = '0'.date('Y');

    	return $reference_number = $prefix.'-'.$year.'-'.'00'.(intval($count_transaction) + 1);
    }

    public static function check_room_availability($date_from, $date_to, $room_type_id){
        $query = DB::SELECT('SELECT * FROM room WHERE id not in( SELECT b.room_id FROM room a INNER JOIN reserved_room b ON a.id = b.room_id INNER JOIN reservation_details c ON c.id = b.reservation_details_id WHERE c.date_from <= "'.$date_from.'" AND c.date_to >= "'.$date_to.'") AND room_type_id = "'.$room_type_id.'"');
    	return $query;
    }

    public static function food_fee($id){
        if($id == 1){
            return DB::table('system_settings')->value('allow_free_breakfast');
        }
        else{
            return 0;
        }
    }
}
